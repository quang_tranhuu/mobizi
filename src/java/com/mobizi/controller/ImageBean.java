/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mobizi.controller;

import com.mobizi.dao.ProductDao;
import com.mobizi.dao.UserDao;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.sql.SQLException;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.servlet.http.Part;

/**
 *
 * @author tran
 */
@ManagedBean
@RequestScoped
public class ImageBean {

    private static final String pathUser = "E:\\JAVA PROJECT\\mobizi\\web\\resources\\default\\img\\img_members\\";
    public ImageBean() {
    }
    //change avatar
    public void changeAvatar(Part file, String addressPath, String username,String pic_old) throws IOException, SQLException, ClassNotFoundException, InstantiationException, IllegalAccessException {
        deleteFile(addressPath + pic_old);
        String newname="";
        switch (file.getContentType()) {
            case "image/jpeg":
                newname+=username+"."+"jpeg";
                break;
            case "image/jpg":
                newname+=username+"."+"jpg";
                break;
            case "image/png":
                newname+=username+"."+"png";
                break;
            default:
                break;
        } 
        
        uploadNewFile(file, addressPath, newname);
        UserDao dao=new UserDao();
        dao.changePicture(username, newname);
        
         
    }
    //change picture product
    public void changeImageProduct(Part file, String addressPath,String name_product,String pic_new) throws IOException, SQLException, ClassNotFoundException, InstantiationException, IllegalAccessException {
//        deleteFile(addressPath + pic_old);
        uploadImageProduct(file, addressPath);
        ProductDao dao=new ProductDao();
        dao.changePicture(name_product, pic_new);
        
         
    }
    //change picture product where product not have picture
//    public void changeImageProduct_Nopic(Part file, String addressPath,String name_product,String pic_new) throws IOException, SQLException, ClassNotFoundException, InstantiationException, IllegalAccessException {
//        uploadImageProduct(file, addressPath);
//        ProductDao dao=new ProductDao();
//        dao.changePicture(name_product, pic_new);
//        
//         
//    }
    //upload image from my computer and set name for image - format image.png -USER
    public void uploadNewFile(Part file, String addressPath, String newname) throws IOException {
        InputStream input = file.getInputStream();
        Files.copy(input, new File(addressPath, file.getSubmittedFileName()).toPath());
        String image = file.getSubmittedFileName();
        renameFile(addressPath + image, addressPath + newname );
    }
    //upload image product
    public void uploadImageProduct(Part file, String addressPath) throws IOException {
        InputStream input = file.getInputStream();
        Files.copy(input, new File(addressPath, file.getSubmittedFileName()).toPath());
        
    }
    
    //upload not rename file
    public void uploadNewFile_NotRename(Part file, String addressPath) throws IOException {
        InputStream input = file.getInputStream();
        Files.copy(input, new File(addressPath, file.getSubmittedFileName()).toPath());
    }
    //rename image - format image.png -USER -POST
    public void renameFile(String oldFilePath, String newFilePath) {
        File oldfile = new File(oldFilePath);
        File newfile = new File(newFilePath);
        oldfile.renameTo(newfile);
    }
    //delete old image -USER 
    public void deleteFile(String filePath) {
        File currentfile = new File(filePath);
        currentfile.delete();
    }
}
