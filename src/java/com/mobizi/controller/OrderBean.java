/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mobizi.controller;

import com.mobizi.dao.OrderDao;
import com.mobizi.entity.Ordertable;
import com.mobizi.entity.Product;
import com.mobizi.entity.User;
import com.mobizi.utils.DateUtil;
import java.math.BigDecimal;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 *
 * @author tran
 */
@ManagedBean
@RequestScoped
public class OrderBean {

    private User user = new User();
    private Ordertable order = new Ordertable();
    private String meserror;

    /**
     * Creates a new instance of OrderBean
     */
    public OrderBean() {
    }

    // make order, if success return homepage
    public String insertOrdertable() throws SQLException, ClassNotFoundException, InstantiationException, IllegalAccessException {
        OrderDao dao = new OrderDao();
        HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
        ArrayList<Product> cart = (ArrayList<Product>) session.getAttribute("ShoppingCart");
        Integer userID = (Integer) session.getAttribute("id_user");
        Date order_date = new Date(); // ngày order
        Date delivery_date = DateUtil.addDays(order_date, 3);//ngày nhận hàng
        String stat = "process";
        String receiver = order.getReceiver();// người nhận
        String address_order = order.getAddress_order(); //địa chỉ nhận
        Long total_price = 0l;
        String tel = order.getTel_receiver();// điện thoại ng nhận
        Long order_total = 0l;
        String description = "";
        for (int k = 0; k < cart.size(); k++) {
            order_total += cart.get(k).getQuantity() * cart.get(k).getPrice();
            description += "+" + cart.get(k).getName() + " - Gía= " + cart.get(k).getPrice() + " - SL : " + cart.get(k).getQuantity()
                    + "\n";
        }
        if (userID != null) {
            if ((!receiver.equalsIgnoreCase("") && !tel.equalsIgnoreCase("") && !address_order.equalsIgnoreCase(""))) {
                dao.addOrder(userID, order_date, order_total, delivery_date, stat, receiver, address_order, total_price, description, tel);
                session.removeAttribute("ShoppingCart");
                meserror="";
                return "homepage_1?faces-redirect=true";
            } else {
                meserror = "Điền đầy đủ vào form";
                return null;
            }
        } else {
            return "login?faces-redirect=true";
        }
        

    }

    //get all order (for admin)

    public ArrayList<Ordertable> getAll_Order() throws SQLException, ClassNotFoundException, InstantiationException, IllegalAccessException {
        OrderDao dao = new OrderDao();
        return dao.list_All_Order();
    }

    //get order of user by id_user
    public ArrayList<Ordertable> getOrder_By_IDuser() throws SQLException, ClassNotFoundException, InstantiationException, IllegalAccessException {
        HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
        Integer id_user = (Integer) session.getAttribute("id_user");
        OrderDao dao = new OrderDao();
        return dao.getOrder_By_IDuser(id_user);
    }

    //count number of order
    public Integer count_Order() throws SQLException, ClassNotFoundException, InstantiationException, IllegalAccessException {
        OrderDao dao = new OrderDao();
        return dao.list_All_Order().size();
    }

    //count order by id_user

    public Integer count_Order_By_IDuser() throws SQLException, ClassNotFoundException, InstantiationException, IllegalAccessException {
        HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
        Integer id_user = (Integer) session.getAttribute("id_user");
        OrderDao dao = new OrderDao();
        return dao.getOrder_By_IDuser(id_user).size();
    }

    //delete order
    public void delete_order() throws SQLException, ClassNotFoundException, InstantiationException, IllegalAccessException {
        HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
        String orderID = request.getParameter("id");
        Integer id = Integer.parseInt(orderID);
        OrderDao dao = new OrderDao();
        dao.delete_Order_by_id(id);
    }
    //duyet order

    public void approval_Order() throws SQLException, ClassNotFoundException, InstantiationException, IllegalAccessException {
        HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
        String orderID = request.getParameter("id1");
        OrderDao dao = new OrderDao();
        dao.approval_Order(Integer.parseInt(orderID));
    }

    //list all 
// SET GET
    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Integer getId_user() {
        return user.getId_user();
    }

    public void setId_user(Integer id_user) {
        user.setId_user(id_user);
    }

    public String getMail() {
        return user.getMail();
    }

    public void setMail(String mail) {
        user.setMail(mail);
    }

    public String getPassword() {
        return user.getPassword();
    }

    public void setPassword(String password) {
        user.setPassword(password);
    }

    public String getUsername() {
        return user.getUsername();
    }

    public void setUsername(String username) {
        user.setUsername(username);
    }

    public Date getBirthday() {
        return user.getBirthday();
    }

    public void setBirthday(Date birthday) {
        user.setBirthday(birthday);
    }

    public String getTel() {
        return user.getTel();
    }

    public void setTel(String tel) {
        user.setTel(tel);
    }

    public String getStatus() {
        return user.getStatus();
    }

    public void setStatus(String status) {
        user.setStatus(status);
    }

    public String getPicture() {
        return user.getPicture();
    }

    public void setPicture(String picture) {
        user.setPicture(picture);
    }

    public Date getLast_access_date() {
        return user.getLast_access_date();
    }

    public void setLast_access_date(Date last_access_date) {
        user.setLast_access_date(last_access_date);
    }

    public String getCity() {
        return user.getCity();
    }

    public void setCity(String city) {
        user.setCity(city);
    }

    public String getAddress() {
        return user.getAddress();
    }

    public void setAddress(String address) {
        user.setAddress(address);
    }

    public String getGender() {
        return user.getGender();
    }

    public void setGender(String gender) {
        user.setGender(gender);
    }

    public Integer getRole() {
        return user.getRole();
    }

    public void setRole(Integer role) {
        user.setRole(role);
    }

    public Integer getId_order() {
        return order.getId_order();
    }

    public void setId_order(Integer id_order) {
        order.setId_order(id_order);
    }

    public Integer getUser_id() {
        return order.getUser_id();
    }

    public void setUser_id(Integer user_id) {
        order.setUser_id(user_id);
    }

    public Date getOrder_date() {
        return order.getOrder_date();
    }

    public void setOrder_date(Date order_date) {
        order.setOrder_date(order_date);
    }

    public Long getOrder_total() {
        return order.getOrder_total();
    }

    public void setOrder_total(Long order_total) {
        order.setOrder_total(order_total);
    }

    public Date getDelivery_date() {
        return order.getDelivery_date();
    }

    public void setDelivery_date(Date delivery_date) {
        order.setDelivery_date(delivery_date);
    }

    public String getStat() {
        return order.getStat();
    }

    public void setStat(String stat) {
        order.setStat(stat);
    }

    public String getReceiver() {
        return order.getReceiver();
    }

    public void setReceiver(String receiver) {
        order.setReceiver(receiver);
    }

    public String getAddress_order() {
        return order.getAddress_order();
    }

    public void setAddress_order(String address_order) {
        order.setAddress_order(address_order);
    }

    public Long getTotal_price() {
        return order.getTotal_price();
    }

    public void setTotal_price(Long total_price) {
        order.setTotal_price(total_price);
    }

    public String getDescription() {
        return order.getDescription();
    }

    public void setDescription(String description) {
        order.setDescription(description);
    }

    public String getTel_receiver() {
        return order.getTel_receiver();
    }

    public void setTel_receiver(String tel_receiver) {
        order.setTel_receiver(tel_receiver);
    }

    public Ordertable getOrder() {
        return order;
    }

    public void setOrder(Ordertable order) {
        this.order = order;
    }

    public String getMeserror() {
        return meserror;
    }

    public void setMeserror(String meserror) {
        this.meserror = meserror;
    }

}
