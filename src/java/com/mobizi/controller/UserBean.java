/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mobizi.controller;

import com.mobizi.dao.UserDao;
import com.mobizi.entity.User;
import java.io.IOException;
import java.sql.Array;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;

/**
 *
 * @author tran
 */
@ManagedBean
@SessionScoped
public class UserBean {

    private Part file;
    private static final String addressPathMem ="E:\\JAVA PROJECT\\mobizi\\web\\resources\\default\\img\\img_members\\";
    private  User user=new User();
    private String picturechange;
    private String passwordnew;
    private String passwordold;
    private String passwordconfirm;
    private String error_old;
    private String error_new;
    private String error_confirm;
    private String error;
    private String errori;
    private String messagep;
    private String messagei;
    private String messageimage;
    private String[] errors;
    public UserBean() {
    }
    
     //check username
    public boolean checkLogin()
            throws SQLException, ClassNotFoundException, InstantiationException, IllegalAccessException {
        User item = getUser_by_username(user.getUsername());
        if (item.getUsername() != null && item.getPassword().equals(user.getPassword())) {
            return true;
        }
        return false;
    }

    //get user by username
    public User getUser_by_username(String username) throws SQLException, ClassNotFoundException, InstantiationException, IllegalAccessException {
        UserDao dao = new UserDao();
        return dao.getUser_by_username(username);
    }
    //get all user
    public ArrayList<User> getAll_User() throws SQLException, ClassNotFoundException, InstantiationException, IllegalAccessException{
        UserDao dao=new UserDao();
        return dao.getAll_user();
    }
    
    //delete user
    public void delete_User_By_ID() throws SQLException, ClassNotFoundException, InstantiationException, IllegalAccessException{
        HttpServletRequest request=(HttpServletRequest)FacesContext.getCurrentInstance().getExternalContext().getRequest();
        String userID = request.getParameter("id");
        UserDao dao=new UserDao();
        dao.delete_User_By_ID(Integer.parseInt(userID));
    }
    
    //banned user by id
    public void banned_User_BY_ID() throws SQLException, ClassNotFoundException, InstantiationException, IllegalAccessException{
        HttpServletRequest request=(HttpServletRequest)FacesContext.getCurrentInstance().getExternalContext().getRequest();
        String userID = request.getParameter("id");
        UserDao dao=new UserDao();
        dao.banned_User_By_ID(Integer.parseInt(userID));
    }
    // login
    public String login() throws SQLException, ClassNotFoundException, InstantiationException, IllegalAccessException {

        if (checkLogin()) {
            if( getUser_by_username(user.getUsername()).getStatus().equalsIgnoreCase("baned")==false){
                HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
                User entity = getUser_by_username(user.getUsername());
                session.setAttribute("id_user",entity.getId_user());//kiem tra lai???
                session.setAttribute("username", entity.getUsername());
                session.setAttribute("role", entity.getRole());
                error="";
                user.setMail(entity.getMail());
                user.setAddress(entity.getAddress());
                user.setBirthday(entity.getBirthday());
                user.setCity(entity.getCity());
                user.setFull_name(entity.getFull_name());
                user.setGender(entity.getGender());
                user.setLast_access_date(entity.getLast_access_date());
                user.setPicture(entity.getPicture());
                user.setTel(entity.getTel());
                user.setRole(entity.getRole());
                user.setUsername(entity.getUsername());
                user.setStatus(entity.getStatus()); 
                if(checkRole()){
                    return "admin/admin_page?faces-redirect=true";
                }else{
                    return "homepage_1?faces-redirect=true";
                }
            }else {
                return "isbanned?faces-redirect=true";
            }
        }else {
                error = "Error: Username or password is wrong";
                return "login?faces-redirect=true";
        }
    }
    //log out
    public String logout() {
        HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
        session.removeAttribute("username");
        session.removeAttribute("id_user");
        session.removeAttribute("role");
        user.setUsername(null);
        UserBean use=new UserBean();
        use.setMessagei("");
        use.setMessageimage("");
        use.setMessagep("");
        FacesContext.getCurrentInstance().getExternalContext().invalidateSession();
        return "homepage_1?faces-redirect=true";
    }
      
    //check session - true:  logined
    public boolean checkSession() {
        HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
        if (session.getAttribute("username") == null) {
            return false;
        } else {
            return true;
        }
    }
    
    //check role
    public boolean checkRole(){
        HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
        if ((Integer) session.getAttribute("role") == 1) {
            return true;// is admin
        }else {
            return  false;// is member
        }
        
    }
    
    //to display member or admin
    public String checkType(){
        HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
        if ((Integer) session.getAttribute("role") == 1) {
            return "Administrator";
        }else {
            return  "Member";
        }
    }
    //check null
    public boolean checkNull(String item){
//        return item!="";
        return !item.equalsIgnoreCase("");
    }
    //update info
    public void update_Info() throws SQLException, ClassNotFoundException, InstantiationException, IllegalAccessException{
        HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
        Integer id_user=(Integer) session.getAttribute("id_user");
        UserDao dao=new UserDao();
        errori="";
        messagei="";
        if(checkNull(user.getFull_name())&&checkNull(user.getMail())&&checkNull(user.getCity())
                &&checkNull(user.getGender())&&checkNull(user.getTel())&&checkNull(user.getAddress())){
            dao.update_Info(id_user,user.getFull_name(), user.getMail(),user.getTel(), user.getCity(), user.getAddress(), user.getGender());
            messagei="Change info success";
            errori="";
        }else errori="Điền đầy đủ vào form";
    }
    
    //check password
    
    
    //check not null
    public boolean checkPassword(String pas){
//        return pas!="";
        return !pas.equalsIgnoreCase("");
    }
    
    //change password
    public void change_Password() throws SQLException, ClassNotFoundException, InstantiationException, IllegalAccessException{
        HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
        String userName=(String) session.getAttribute("username");
        User entity=findUser_by_Username(userName);
        UserDao dao=new UserDao();
        error="";
        error_old="";
        error_new="";
        error_confirm="";
        messagep="";
        if(checkPassword(passwordold)&&checkPassword(passwordnew)&&checkPassword(passwordconfirm)){
            if( !entity.getPassword().equalsIgnoreCase(passwordold)){
                error_old="Password cũ sai,mời nhập lại";
            }
            if(passwordnew.length()<6){
                    error_new="Password phải nhiều hơn 6 kí tự ";
            }
            if(!passwordnew.equalsIgnoreCase(passwordconfirm)){
                        error_confirm="Phải nhập đúng password mới";
            }
        }else{
            error="Password không được rỗng";
        }
        if(error==""&&error_old==""&&error_new==""&error_confirm==""){
            dao.changePassword(passwordnew, entity.getId_user());
            error="";
            messagep="change password success";
        }
        
    }
    //change avatar
    public void changeAvatar() throws IOException, SQLException, ClassNotFoundException, InstantiationException, IllegalAccessException {
        HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
        String userName=(String) session.getAttribute("username");
        String picture_old=findUser_by_Username(userName).getPicture();
        ImageBean image = new ImageBean();
        image.changeAvatar(getFile(), addressPathMem, userName,picture_old);
        user.setPicture(findUser_by_Username(userName).getPicture());
        messageimage = "Upload Image and Change Avatar successfully!";
        
    }

    //get user by id
    public User findUser_by_Username(String username) throws SQLException, ClassNotFoundException, InstantiationException, IllegalAccessException{
        UserDao dao=new UserDao();
        return dao.getUser_by_username(username);
    }
    
     // login
    public String login_Admin() throws SQLException, ClassNotFoundException, InstantiationException, IllegalAccessException {

        if (checkLogin()) {
            HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
            
            User entity = getUser_by_username(user.getUsername());
            session.setAttribute("id_user",entity.getId_user());//kiem tra lai???
            session.setAttribute("username", entity.getUsername());
            error="";
            session.setAttribute("role", entity.getRole());
            user.setMail(entity.getMail());
            user.setAddress(entity.getAddress());
            user.setBirthday(entity.getBirthday());
            user.setCity(entity.getCity());
            user.setFull_name(entity.getFull_name());
            user.setGender(entity.getGender());
            user.setLast_access_date(entity.getLast_access_date());
            user.setPicture(entity.getPicture());
            user.setTel(entity.getTel());
            user.setRole(entity.getRole());
            user.setUsername(entity.getUsername());
            user.setStatus(entity.getStatus()); 
            if(checkRole()){         
                    return "list_product?faces-redirect=true";
            }else {return null;}
        }else {
            error = "Error: Username or password is wrong";
            return "login?faces-redirect=true";
        }
        
    }
   
    public String logout_Admin() {
        HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
        session.removeAttribute("username");
        session.removeAttribute("id_user");
        session.removeAttribute("role");
        
        user.setUsername(null);
        FacesContext.getCurrentInstance().getExternalContext().invalidateSession();
        return "login?faces-redirect=true";
    }
    //SET GET

    public Integer getId_user() {
        return user.getId_user();
    }

    public void setId_user(Integer id_user) {
        user.setId_user(id_user);
    }

    public String getFull_name() {
        return user.getFull_name();
    }

    public void setFull_name(String full_name) {
        user.setFull_name(full_name);
    }

    
    public String getMail() {
        return user.getMail();
    }

    public void setMail(String mail) {
        user.setMail(mail);
    }

    public String getPassword() {
        return user.getPassword();
    }

    public void setPassword(String password) {
        user.setPassword(password);
    }

    public String getUsername() {
        return user.getUsername();
    }

    public void setUsername(String username) {
        user.setUsername(username);
    }

    public Date getBirthday() {
        return user.getBirthday();
    }

    public void setBirthday(Date birthday) {
        user.setBirthday(birthday);
    }

    public String getTel() {
        return user.getTel();
    }

    public void setTel(String tel) {
        user.setTel(tel);
    }

    public String getStatus() {
        return user.getStatus();
    }

    public void setStatus(String status) {
        user.setStatus(status);
    }

    public String getPicture() {
        return user.getPicture();
    }

    public void setPicture(String picture) {
        user.setPicture(picture);
    }

    public Date getLast_access_date() {
        return user.getLast_access_date();
    }

    public void setLast_access_date(Date last_access_date) {
        user.setLast_access_date(last_access_date);
    }

    

    public String getCity() {
        return user.getCity();
    }

    public void setCity(String city) {
        user.setCity(city);
    }

    public String getAddress() {
        return user.getAddress();
    }

    public void setAddress(String address) {
        user.setAddress(address);
    }

    public String getGender() {
        return user.getGender();
    }

    public void setGender(String gender) {
        user.setGender(gender);
    }

  

    public Integer getRole() {
        return user.getRole();
    }

    public void setRole(Integer role) {
        user.setRole(role);
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getPasswordnew() {
        return passwordnew;
    }

    public void setPasswordnew(String passwordnew) {
        this.passwordnew = passwordnew;
    }

    public String getError_old() {
        return error_old;
    }

    public void setError_old(String error_old) {
        this.error_old = error_old;
    }

    public String getError_new() {
        return error_new;
    }

    public void setError_new(String error_new) {
        this.error_new = error_new;
    }

    public String getError_confirm() {
        return error_confirm;
    }

    public void setError_confirm(String error_confirm) {
        this.error_confirm = error_confirm;
    }

    public String getErrori() {
        return errori;
    }

    public void setErrori(String errori) {
        this.errori = errori;
    }

    public String getMessagep() {
        return messagep;
    }

    public void setMessagep(String messagep) {
        this.messagep = messagep;
    }

    public String getMessagei() {
        return messagei;
    }

    public void setMessagei(String messagei) {
        this.messagei = messagei;
    }

    

    public String getPasswordconfirm() {
        return passwordconfirm;
    }

    public void setPasswordconfirm(String passwordconfirm) {
        this.passwordconfirm = passwordconfirm;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getPasswordold() {
        return passwordold;
    }

    public void setPasswordold(String passwordold) {
        this.passwordold = passwordold;
    }

    public String[] getErrors() {
        return errors;
    }

    public void setErrors(String[] errors) {
        this.errors = errors;
    }

    public Part getFile() {
        return file;
    }

    public void setFile(Part file) {
        this.file = file;
    }

    public String getMessageimage() {
        return messageimage;
    }

    public void setMessageimage(String messageimage) {
        this.messageimage = messageimage;
    }

    public String getPicturechange() {
        return picturechange;
    }

    public void setPicturechange(String picturechange) {
        this.picturechange = picturechange;
    }
    
    //    public String edit_info() throws SQLException, ClassNotFoundException, InstantiationException, IllegalAccessException{
//        HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
//        String userName=(String) session.getAttribute("username");
//        User use=findUser_by_Username(userName);
//        user.setUsername(use.getUsername());
//        user.setFull_name(use.getFull_name());
//        user.setAddress(use.getAddress());
//        user.setCity(use.getCity());
//        user.setMail(use.getMail());
//        user.setTel(use.getTel());
//        user.setGender(use.getGender());
        
//        return "change_profile?faces-redirect=true";
//    }
    
}
