/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mobizi.controller;

import com.mobizi.dao.CategoryDao;
import com.mobizi.entity.Category;
import com.mobizi.entity.CategoryParent;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

/**
 *
 * @author tran
 */
@ManagedBean
@SessionScoped
public class CategoryBean {

    private Category cate=new Category();
    private CategoryParent catepr=new CategoryParent();
    
    
    //find all category by id_parent
    public ArrayList<Category> getCategoriesByParent(int id_parent) throws SQLException, ClassNotFoundException, InstantiationException, IllegalAccessException {
        CategoryDao dao = new CategoryDao();
        return dao.getCategory_By_Idparent(id_parent);
    }
    
    //get all parent category
    public ArrayList<CategoryParent> getAllCategoryParent() throws SQLException, ClassNotFoundException, InstantiationException, IllegalAccessException{
        CategoryDao dao=new CategoryDao();
        return dao.getAll_ParentCategory();
    }
    public CategoryBean() {
    }

    public Category getCate() {
        return cate;
    }

    public void setCate(Category cate) {
        this.cate = cate;
    }

    public CategoryParent getCatepr() {
        return catepr;
    }

    public void setCatepr(CategoryParent catepr) {
        this.catepr = catepr;
    }
    
    
}
