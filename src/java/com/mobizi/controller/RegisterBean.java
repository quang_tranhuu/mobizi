/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mobizi.controller;

import com.mobizi.dao.UserDao;
import com.mobizi.entity.User;
import java.sql.SQLException;
import java.util.Date;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import org.apache.tomcat.dbcp.pool2.PoolUtils;

/**
 *
 * @author tran
 */
@ManagedBean
@RequestScoped
public class RegisterBean {

    private User user = new User();
    private String erros;
    private String erros1;
    private String erros2;
    private String errornull;
    private String password1;

    public RegisterBean() {

    }

    public String register() throws SQLException, ClassNotFoundException, InstantiationException, IllegalAccessException {
        UserDao dao = new UserDao();
        erros = null;
        erros1 = null;
        erros2=null;
        errornull=null;
        if( !user.getUsername().equalsIgnoreCase("") && !user.getPassword().equalsIgnoreCase("") && !user.getMail().equalsIgnoreCase("") && !password1.equalsIgnoreCase("")){
            
            User item = getUser_by_username(user.getUsername());
            User item1 = getUser_by_Mail(user.getMail());

            if (item.getUsername() != null) {
                erros = "username da ton tai";
                
            }
            if (!user.getPassword().equals(password1)) {
                erros1 = "Password phai giong nhau";
            }
            if (item1.getMail()!=null) {
                erros2="Email nay da ton tai";
            }
            if(erros==null && erros1==null && erros2==null ){
                dao.addUser(user.getUsername(), user.getPassword(), user.getMail());
                erros = "";
                erros1 = "";
                erros2 = "";
                return "login?faces-redirect=true";
            }
            return null;
        }else{
           errornull="Điền đầy đủ vào form ";
            return null;
        }
    }

    //get user by username
    public User getUser_by_username(String username) throws SQLException, ClassNotFoundException, InstantiationException, IllegalAccessException {
        UserDao dao = new UserDao();
        return dao.getUser_by_username(username);
    }
//get user by mail

    public User getUser_by_Mail(String mail) throws SQLException, ClassNotFoundException, InstantiationException, IllegalAccessException {
        UserDao dao = new UserDao();
        return dao.getUser_by_Mail(mail);
    }

    //SET GET
    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getErros() {
        return erros;
    }

    public void setErros(String erros) {
        this.erros = erros;
    }

    public String getErros1() {
        return erros1;
    }

    public void setErros1(String erros1) {
        this.erros1 = erros1;
    }

    public String getErros2() {
        return erros2;
    }

    public void setErros2(String erros2) {
        this.erros2 = erros2;
    }

    public String getErrornull() {
        return errornull;
    }

    public void setErrornull(String errornull) {
        this.errornull = errornull;
    }
    

    
    public String getPassword1() {
        return password1;
    }

    public void setPassword1(String password1) {
        this.password1 = password1;
    }

    public Integer getId_user() {
        return user.getId_user();
    }

    public void setId_user(Integer id_user) {
        user.setId_user(id_user);
    }

    public String getMail() {
        return user.getMail();
    }

    public void setMail(String mail) {
        user.setMail(mail);
    }

    public String getPassword() {
        return user.getPassword();
    }

    public void setPassword(String password) {
        user.setPassword(password);
    }

    public String getUsername() {
        return user.getUsername();
    }

    public void setUsername(String username) {
        user.setUsername(username);
    }

    public Date getBirthday() {
        return user.getBirthday();
    }

    public void setBirthday(Date birthday) {
        user.setBirthday(birthday);
    }

    public String getTel() {
        return user.getTel();
    }

    public void setTel(String tel) {
        user.setTel(tel);
    }

    public String getStatus() {
        return user.getStatus();
    }

    public void setStatus(String status) {
        user.setStatus(status);
    }

    public String getPicture() {
        return user.getPicture();
    }

    public void setPicture(String picture) {
        user.setPicture(picture);
    }

    public Date getLast_access_date() {
        return user.getLast_access_date();
    }

    public void setLast_access_date(Date last_access_date) {
        user.setLast_access_date(last_access_date);
    }

    

    public String getCity() {
        return user.getCity();
    }

    public void setCity(String city) {
        user.setCity(city);
    }

    public String getAddress() {
        return user.getAddress();
    }

    public void setAddress(String address) {
        user.setAddress(address);
    }

    public String getGender() {
        return user.getGender();
    }

    public void setGender(String gender) {
        user.setGender(gender);
    }

    

    public Integer getRole() {
        return user.getRole();
    }

    public void setRole(Integer role) {
        user.setRole(role);
    }

}
