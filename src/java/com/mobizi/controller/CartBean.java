/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mobizi.controller;

import com.mobizi.dao.ProductDao;
import com.mobizi.entity.Product;
import com.mobizi.entity.SubProduct;
import com.mobizi.entity.User;
import static com.sun.corba.se.spi.presentation.rmi.StubAdapter.request;
import com.sun.faces.el.ELConstants;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.sql.Array;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.faces.event.AjaxBehaviorEvent;

/**
 *
 * @author tran
 */
@ManagedBean
@RequestScoped
public class CartBean {

    private Product product;
    private ArrayList<Product> cartlist;
    private Long total;
    private Integer size;
    private User user=new User();
    
    public CartBean() {
    }
    

// get product by id
    public Product getProduct_by_ID(Integer id_product) throws SQLException, ClassNotFoundException, InstantiationException, IllegalAccessException {
        ProductDao dao = new ProductDao();
        return dao.getProduct_by_id(id_product);

    }

// add item to cart in page detail product
    public String addCart()throws IOException, ServletException, SQLException, ClassNotFoundException, InstantiationException, IllegalAccessException {
        HttpServletRequest request=(HttpServletRequest)FacesContext.getCurrentInstance().getExternalContext().getRequest();
        HttpServletResponse response = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse();
// First get the item values from the request.
        String productID = request.getParameter("id");
// Now create an item to add to the cart.
        Product item = getProduct_by_ID(Integer.parseInt(productID));
        HttpSession session = request.getSession();
// Get the cart.
        ArrayList<Product> cart = (ArrayList<Product>) session.getAttribute("ShoppingCart");
        Boolean has=false;
// If there is no shopping cart, create one.
        if (cart == null) {
            cart = new ArrayList<>();            
            session.setAttribute("ShoppingCart", cart);         
        }
        Integer index=0;
        for (Integer k=0; k< cart.size(); k++) {
            if (cart.get(k).getId_product()==Integer.parseInt(productID))
                   {
                has = true;
                index=k;
                break;
            }           
        }
        if(cart.size()<10){
            if (!has) {
                item.setQuantity(1);
                item.setTemp(item.getQuantity()*item.getPrice());
                cart.add(item);
            }else{
                Integer n=cart.get(index).getQuantity();
                Long t=cart.get(index).getPrice();
                cart.get(index).setQuantity(n+1);
                cart.get(index).setTemp((n+1)*t);
            }
        }
        session.setAttribute("ShoppingCart", cart);//moi them
        
        return "homepage_1?faces-redirect=true";
    }
    
    
    //increase quantity of product in cart
    public void plusQuantity(Product product){
        HttpServletRequest request=(HttpServletRequest)FacesContext.getCurrentInstance().getExternalContext().getRequest();
//        String productID = request.getParameter("id_product");
        HttpSession session = request.getSession();
//        product.getId_product();
        ArrayList<Product> cart = (ArrayList<Product>) session.getAttribute("ShoppingCart");
        
        for (Product p : cart) {
            if (p.getId_product()==product.getId_product()){
                if(p.getQuantity()<10){                   
                p.setQuantity(p.getQuantity() + 1);
                p.setTemp(p.getQuantity()*p.getPrice());
                p.setIntroduce(null);
                break;
            }   else p.setIntroduce("SL < 10");          
        }
        }
        
         
        session.setAttribute("ShoppingCart", cart);
        
    }
    
    //decrease quantity of product in cart
    public void subQuantity(Product product){
        HttpServletRequest request=(HttpServletRequest)FacesContext.getCurrentInstance().getExternalContext().getRequest();
//        String productID = request.getParameter("id_product");
        HttpSession session = request.getSession(); 
//        product.getId_product();
        ArrayList<Product> cart = (ArrayList<Product>) session.getAttribute("ShoppingCart");
        for (Product p : cart) {
            if (p.getId_product()==product.getId_product()){
                if(p.getQuantity()>1){
                p.setQuantity(p.getQuantity() - 1);
                p.setTemp(p.getQuantity()*p.getPrice());
                p.setIntroduce("");
                break;
                }else p.setIntroduce("SL > 0");            
        }
        }
        
        session.setAttribute("ShoppingCart", cart);
        
    }
    
// delete item in cart
    public String deleteItem_in_Cart(){
        HttpServletRequest request=(HttpServletRequest)FacesContext.getCurrentInstance().getExternalContext().getRequest();
        String productID = request.getParameter("id_product");
        HttpSession session = request.getSession();
        ArrayList<Product> shoppingCart = (ArrayList<Product>) session.getAttribute("ShoppingCart");
        int index = -1;
        for (Integer k=0; k< shoppingCart.size(); k++) {
            if (shoppingCart.get(k).getId_product()==Integer.parseInt(productID)) {
                index = k;
                break;
            }
        }
        if (index != -1) {
            shoppingCart.remove(index);
            session.setAttribute("ShoppingCart", shoppingCart);  
        }
        return "cart?faces-redirect=true";
    }
    
    //ajax tinh sum
    public void countSummary(AjaxBehaviorEvent event){
        HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
        ArrayList<Product> listcart = (ArrayList<Product>) session.getAttribute("ShoppingCart");
         total=0l;
        for(Product p: listcart){
            total += p.getTemp();
        }
//        total=total1;
    }
// get item in cart    
    public Product getCart_item(Integer id_product){
        HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
        ArrayList<Product> cart = (ArrayList<Product>) session.getAttribute("ShoppingCart");
        HttpServletRequest request=(HttpServletRequest)FacesContext.getCurrentInstance().getExternalContext().getRequest();
        String productID = request.getParameter("id");
        for (Integer k=0; k< cart.size(); k++) {
            if (cart.get(k).getId_product()==Integer.parseInt(productID)){
                return cart.get(k);                
            }            
        }
        return null;
    }
    
// get list cart    
    public ArrayList<Product> getListCart() {
        ArrayList<Product> list = new ArrayList<>();
        HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
        return list = (ArrayList<Product>) session.getAttribute("ShoppingCart");
    }

// get size cart to display number product in giỏ hàng
    public Integer getSizeCart(){
      
        HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
        ArrayList<Product> listcart = (ArrayList<Product>) session.getAttribute("ShoppingCart");
        if (listcart == null) {
            listcart = new ArrayList<>();            
            session.setAttribute("ShoppingCart", listcart);         
        }
//        Integer number;
//        number=listcart.size();
        return listcart.size();
    }
    
    //insert to cart when click on button "MUA"
     public String insertCart()throws IOException, ServletException, SQLException, ClassNotFoundException, InstantiationException, IllegalAccessException {
        HttpServletRequest request=(HttpServletRequest)FacesContext.getCurrentInstance().getExternalContext().getRequest();
// First get the item values from the request.
        String productID = request.getParameter("id");
// Now create an item to add to the cart.
        Product item = getProduct_by_ID(Integer.parseInt(productID));
        HttpSession session = request.getSession();
// Get the cart.
        ArrayList<Product> cart = (ArrayList<Product>) session.getAttribute("ShoppingCart");
        Boolean has=false;
// If there is no shopping cart, create one.
        if (cart == null) {
            cart = new ArrayList<>();            
            session.setAttribute("ShoppingCart", cart);         
        }
        Integer index=0;
        for (Integer k=0; k< cart.size(); k++) {
            if (cart.get(k).getId_product()==Integer.parseInt(productID))
                   {
                has = true;
                index=k;
                break;
            }           
        }
        if(cart.size()<10){
            if (!has) {
                item.setQuantity(1);
                item.setTemp(item.getQuantity()*item.getPrice());
                cart.add(item);
            }else{
                Integer n=cart.get(index).getQuantity();
                Long t=cart.get(index).getPrice();
                cart.get(index).setQuantity(n+1);
                cart.get(index).setTemp((n+1)*t);
            }
        }
        cartlist=cart;
        session.setAttribute("ShoppingCart", cart);   
        
        return "cart?faces-redirect=true";
    }
        
   

// SET & GET
    
    public Integer getSize() {
        return size;
    }
    
    public void setSize(Integer size) {
        this.size = size;
    }

    public Long getTotal() {
        return total;
    }

    public void setTotal(Long total) {
        this.total = total;
    }

    public Integer getId_product() {
        return product.getId_product();
    }

    public void setId_product(Integer id_product) {
        product.setId_product(id_product);
    }

    public Integer getId_user() {
        return user.getId_user();
    }

    public void setId_user(Integer id_user) {
        user.setId_user(id_user);
    }

    public String getMail() {
        return user.getMail();
    }

    public void setMail(String mail) {
        user.setMail(mail);
    }

    public String getPassword() {
        return user.getPassword();
    }

    public void setPassword(String password) {
        user.setPassword(password);
    }

    public String getUsername() {
        return user.getUsername();
    }

    public void setUsername(String username) {
        user.setUsername(username);
    }

    public Date getBirthday() {
        return user.getBirthday();
    }

    public void setBirthday(Date birthday) {
        user.setBirthday(birthday);
    }

    public String getTel() {
        return user.getTel();
    }

    public void setTel(String tel) {
        user.setTel(tel);
    }

    public String getStatus() {
        return user.getStatus();
    }

    public void setStatus(String status) {
        user.setStatus(status);
    }

    public String getPicture() {
        return user.getPicture();
    }

    public void setPicture(String picture) {
        user.setPicture(picture);
    }

    public Date getLast_access_date() {
        return user.getLast_access_date();
    }

    public void setLast_access_date(Date last_access_date) {
        user.setLast_access_date(last_access_date);
    }
   

    public String getCity() {
        return user.getCity();
    }

    public void setCity(String city) {
        user.setCity(city);
    }

    public String getAddress() {
        return user.getAddress();
    }

    public void setAddress(String address) {
        user.setAddress(address);
    }

    public String getGender() {
        return user.getGender();
    }

    public void setGender(String gender) {
        user.setGender(gender);
    }

    public Integer getRole() {
        return user.getRole();
    }

    public void setRole(Integer role) {
        user.setRole(role);
    }

    public ArrayList<Product> getCartlist() {
        return cartlist;
    }

    public void setCartlist(ArrayList<Product> cartlist) {
        this.cartlist = cartlist;
    }

    
    
    
    
    
//    public void setQuantity1(Integer quantity1) {
//        this.quantity1 = quantity1;
//        HttpServletRequest request=(HttpServletRequest)FacesContext.getCurrentInstance().getExternalContext().getRequest();
//        HttpServletResponse response = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse();
//        String productID = request.getParameter("id_product");
//        if(quantity1>=0 && quantity1<=10){
//            total=getCart_item(Integer.parseInt(productID)).getPrice()*quantity1;
//            
//        }
//    }
//    public String getNumm(){
//        
//		if (num == null) {
//			return "Enter Amount";
//		}else if(num >10 & num <0){
//		return "Amount must be >0 & <10";}
//        return null;
//	}
 //    public int[] getnumbe() {
//        int[] a = new int[10];
//        for (int i = 1; i <= 10; i++) {
//            a[i] = i;
//
//        }
//        return a;
//    }
   
    
    /**
     * Returns a Vector containing the items in the cart. The Vector returned is
     * a clone, so modifying the vector won't affect the contents of the cart.
     */


//    public Product getItems(int itemIndex) {
//        return (Vector) items.clone();
//        return cart.get(itemIndex);
//    }
//
//    public void addItem(Product newItem) {
//        items.addElement(newItem);
//        cart.add(newItem);
//    }
//
//    public void removeItem(int itemIndex) {
//        items.removeElementAt(itemIndex);
//        cart.remove(itemIndex);
//    }
    
//    public String getMess() {
//        return mess;
//    }
//
//    public void setMess(String mess) {
//        this.mess = mess;
//    }
//   
   
    // caculate total    
//    public Long getSummary(){
//        HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
//        ArrayList<Product> listcart = (ArrayList<Product>) session.getAttribute("ShoppingCart");
//        Long total1=0l;
//        for(Product p: listcart){
//            total1 += p.getTemp();
//        }
//        return total1;
//    }

}
