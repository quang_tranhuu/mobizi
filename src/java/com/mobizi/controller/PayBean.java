/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mobizi.controller;

import com.mobizi.entity.Ordertable;
import com.mobizi.entity.User;
import java.util.Date;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

/**
 *
 * @author tran
 */
@ManagedBean
@RequestScoped
public class PayBean {

    private User user;
    private Ordertable order;
    public PayBean() {
    }
    
    
    
    //SET GET

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Ordertable getOrder() {
        return order;
    }

    public void setOrder(Ordertable order) {
        this.order = order;
    }

    public Integer getId_user() {
        return user.getId_user();
    }

    public void setId_user(Integer id_user) {
        user.setId_user(id_user);
    }

    public String getMail() {
        return user.getMail();
    }

    public void setMail(String mail) {
        user.setMail(mail);
    }

    public String getPassword() {
        return user.getPassword();
    }

    public void setPassword(String password) {
        user.setPassword(password);
    }

    public String getUsername() {
        return user.getUsername();
    }

    public void setUsername(String username) {
        user.setUsername(username);
    }

    public Date getBirthday() {
        return user.getBirthday();
    }

    public void setBirthday(Date birthday) {
        user.setBirthday(birthday);
    }

    public String getTel() {
        return user.getTel();
    }

    public void setTel(String tel) {
        user.setTel(tel);
    }

    public String getStatus() {
        return user.getStatus();
    }

    public void setStatus(String status) {
        user.setStatus(status);
    }

    public String getPicture() {
        return user.getPicture();
    }

    public void setPicture(String picture) {
        user.setPicture(picture);
    }

    public Date getLast_access_date() {
        return user.getLast_access_date();
    }

    public void setLast_access_date(Date last_access_date) {
        user.setLast_access_date(last_access_date);
    }

    

    public String getCity() {
        return user.getCity();
    }

    public void setCity(String city) {
        user.setCity(city);
    }

    public String getAddress() {
        return user.getAddress();
    }

    public void setAddress(String address) {
        user.setAddress(address);
    }

    public String getGender() {
        return user.getGender();
    }

    public void setGender(String gender) {
        user.setGender(gender);
    }

    

    public Integer getRole() {
        return user.getRole();
    }

    public void setRole(Integer role) {
        user.setRole(role);
    }

    public Integer getId_order() {
        return order.getId_order();
    }

    public void setId_order(Integer id_order) {
        order.setId_order(id_order);
    }

    public Integer getUser_id() {
        return order.getUser_id();
    }

    public void setUser_id(Integer user_id) {
        order.setUser_id(user_id);
    }

    public Date getOrder_date() {
        return order.getOrder_date();
    }

    public void setOrder_date(Date order_date) {
        order.setOrder_date(order_date);
    }

    public Long getOrder_total() {
        return order.getOrder_total();
    }

    public void setOrder_total(Long order_total) {
        order.setOrder_total(order_total);
    }

    public Date getDelivery_date() {
        return order.getDelivery_date();
    }

    public void setDelivery_date(Date delivery_date) {
        order.setDelivery_date(delivery_date);
    }

    public String getStat() {
        return order.getStat();
    }

    public void setStat(String stat) {
        order.setStat(stat);
    }

    public String getReceiver() {
        return order.getReceiver();
    }

    public void setReceiver(String receiver) {
        order.setReceiver(receiver);
    }

    public String getAddress_order() {
        return order.getAddress_order();
    }

    public void setAddress_order(String address_order) {
        order.setAddress_order(address_order);
    }

    public Long getTotal_price() {
        return order.getTotal_price();
    }

    public void setTotal_price(Long total_price) {
        order.setTotal_price(total_price);
    }

    public String getDescription() {
        return order.getDescription();
    }

    public void setDescription(String description) {
        order.setDescription(description);
    }

    
    
    
}
