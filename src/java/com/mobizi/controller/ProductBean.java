/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mobizi.controller;

import com.mobizi.dao.CategoryDao;
import com.mobizi.dao.OrderDao;
import com.mobizi.dao.ProductDao;
import com.mobizi.entity.Category;
import com.mobizi.entity.CategoryParent;
import com.mobizi.entity.Product;
import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Array;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Map;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.AjaxBehaviorEvent;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;

/**
 *
 * @author tran
 */
@ManagedBean
@SessionScoped
public class ProductBean {

    private Part file;
    private static final String addressPathImage ="E:\\JAVA PROJECT\\mobizi\\web\\resources\\default\\img\\img_system\\img_thumbnail\\";
    private ArrayList<Category> listcategory=new ArrayList<>();
    private Product product = new Product();
    private ArrayList<Category> listcate=new ArrayList<>();// use for edit product page
    private ArrayList<Product> listproductbycate=new ArrayList<>();
    private String messimgproduct;
    private String imageajax;
    private String cate;
    private Integer viewnum;
    private Integer num;
    private String mes;
    private String namenew;
    private Integer cate_idnew;
    private Long pricenew;
    private Integer promotionnew;
    private Integer quantitynew;
    private String manufaternew;
    private Integer hidenew;
    private Integer pd_ID;
            
    public ProductBean() {
    }

    //get all product
    public ArrayList<Product> getAllProduct() throws SQLException, ClassNotFoundException, InstantiationException, IllegalAccessException {
        ProductDao dao = new ProductDao();
        return dao.getAllProduct();
    }

    //get product hot limit by num
    public ArrayList<Product> getHot(Integer num) throws SQLException, ClassNotFoundException, InstantiationException, IllegalAccessException {
        ProductDao dao = new ProductDao();
        return dao.getHot(num);
    }
    
    //get hot product of each category parent 
    public ArrayList<Product> getHot_by_CategoryBean(Integer num,Integer id_parent) throws SQLException, ClassNotFoundException, InstantiationException, IllegalAccessException {
        ProductDao dao = new ProductDao();
        return dao.getHot_by_Category(num,id_parent);
    }

    //get all product by name category parent
    public String getAllProductByCategory(String category) throws SQLException, ClassNotFoundException, InstantiationException, IllegalAccessException {
        setCate(category);
        ProductDao dao = new ProductDao();
        listproductbycate=dao.getAllProductByCategory(category);
        return "viewsproduct?faces-redirect=true";
    }

    //get all category
    public ArrayList<Category> getAllCategory() throws SQLException, ClassNotFoundException, InstantiationException, IllegalAccessException {
        CategoryDao dao = new CategoryDao();
        return dao.getAllCategory();
    }
    
    //get all son category of parent category ,using id_parent
    public ArrayList<Category> getAll_SubCategory(Integer id_parent) throws SQLException, ClassNotFoundException, InstantiationException, IllegalAccessException {
        CategoryDao dao = new CategoryDao();
//        ArrayList<Category> sub=dao.getAll_SubCategory(id);
        return dao.getAll_SubCategory(id_parent);
    }
    
    
    //get all parent category
    public ArrayList<CategoryParent> getAll_ParentCategory() throws SQLException, ClassNotFoundException, InstantiationException, IllegalAccessException{
        CategoryDao dao=new CategoryDao();
        ArrayList<CategoryParent> list=dao.getAll_ParentCategory();
        return list;
    }
    
    public ArrayList<Product> getAllProduct_By_SubCategory(Integer category) throws SQLException, ClassNotFoundException, InstantiationException, IllegalAccessException {
        ProductDao dao = new ProductDao();
        ArrayList<Product> rs=new ArrayList<>();
        rs=dao.getAllProduct_By_SubCategory(category);
        return rs;
    }
    //get all subcategory by id_category of 1 product
    public ArrayList<Category> getAll_SubCategory_by_IDsub(Integer id_category) throws SQLException, ClassNotFoundException, InstantiationException, IllegalAccessException{
        CategoryDao dao=new CategoryDao();
    //get item of category by id_category
        Category item=dao.getCategory_by_IdCategory(id_category);
    //return all subcategory    
        return dao.getAll_SubCategory(item.getId_parent());
        
    }
    
    //get product by id_product
    public Product getProduct_byID(Integer id) throws SQLException, ClassNotFoundException, InstantiationException, IllegalAccessException {
        ProductDao dao = new ProductDao();
        Product product = dao.getProduct_by_id(id);

        return product;
    }
    //get product by name
    public Product getProduct_byName(String name) throws SQLException, ClassNotFoundException, InstantiationException, IllegalAccessException {
        ProductDao dao = new ProductDao();
        Product product = dao.getProduct_by_Name(name);
        return product;
    }
    //count number of product by category
    public Integer countProduct_By_Category(String category) throws SQLException, ClassNotFoundException, InstantiationException, IllegalAccessException{
        ProductDao dao=new ProductDao();
        return dao.getAllProductByCategory(category).size();
    }
//    public boolean checkRender(String param){
//        return param.equalsIgnoreCase("");
//    }
    public String detailProduct() throws SQLException, ClassNotFoundException, InstantiationException, IllegalAccessException{
        HttpServletRequest request=(HttpServletRequest)FacesContext.getCurrentInstance().getExternalContext().getRequest();
        String productID = request.getParameter("id");
        Product pd= getProduct_byID(Integer.parseInt(productID));
        product.setId_product(pd.getId_product());
        product.setName(pd.getName());
        product.setCategory_id(pd.getCategory_id());
        product.setPrice(pd.getPrice());
        product.setPrice_promotion(pd.getPrice_promotion());
        product.setQuantity(pd.getQuantity());
        product.setManufacter(pd.getManufacter());
        product.setHide(pd.getHide());
        product.setMainImageURL(pd.getMainImageURL());
        product.setViews(pd.getViews()+1);
        ProductDao dao=new ProductDao();
        dao.update_Views(pd.getId_product(), pd.getViews());
        return "detailproduct?faces-redirect=true";
    }
    
    //update product
    public String update_Product() throws SQLException, ClassNotFoundException, InstantiationException, IllegalAccessException{
        HttpServletRequest request=(HttpServletRequest)FacesContext.getCurrentInstance().getExternalContext().getRequest();
        String productID = request.getParameter("productID");
        ProductDao dao=new ProductDao();
        dao.update_Product(Integer.parseInt(productID),product.getName(),product.getCategory_id(),product.getPrice(),product.getPrice_promotion(),product.getManufacter(),product.getQuantity(),product.getHide());
        mes="Update thành công";
        return "list_product?faces-redirect=true";
        
        
    }
    
    //insert product
    public void insertNew_Product() throws SQLException, ClassNotFoundException, InstantiationException, IllegalAccessException, IOException{
        ImageBean image = new ImageBean();
        String name_new=file.getSubmittedFileName();
        image.uploadImageProduct(file, addressPathImage);
        ProductDao dao=new ProductDao();
        dao.insertNew_Product(product.getName(), product.getCategory_id(), product.getPrice(), product.getPrice_promotion(), product.getManufacter(), product.getQuantity(), product.getHide(),name_new);
        
    }
    

//    


    //edit product
    public String edit_Product() throws SQLException, ClassNotFoundException, InstantiationException, IllegalAccessException{
        HttpServletRequest request=(HttpServletRequest)FacesContext.getCurrentInstance().getExternalContext().getRequest();
        String productID = request.getParameter("id");
        Product pd= getProduct_byID(Integer.parseInt(productID));
        product.setId_product(pd.getId_product());
        product.setName(pd.getName());
        product.setCategory_id(pd.getCategory_id());
        product.setPrice(pd.getPrice());
        product.setPrice_promotion(pd.getPrice_promotion());
        product.setQuantity(pd.getQuantity());
        product.setManufacter(pd.getManufacter());
        product.setHide(pd.getHide());
        ArrayList<Category> list= getAll_SubCategory_by_IDsub(pd.getCategory_id());
//        for (Category item : list) {
//            Category entity= new Category();
//            entity.setId_category(item.getId_category());
//            entity.setName(item.getName());
//            listcate.add(entity);
//        }
        listcate=list;
        return "edit_product?faces-redirect=true";
    }
    //delete product by id
    public void delete_Product() throws SQLException, ClassNotFoundException, InstantiationException, IllegalAccessException{
        HttpServletRequest request=(HttpServletRequest)FacesContext.getCurrentInstance().getExternalContext().getRequest();
        String productID = request.getParameter("id");
        Integer id=Integer.parseInt(productID);
        ProductDao dao=new ProductDao();
        dao.delete_Product_by_ID(id);
    }
    
    //change picture product
    
    public void changePicture() throws IOException, SQLException, ClassNotFoundException, InstantiationException, IllegalAccessException {
        String name_product=product.getName();
        String pic_old=getProduct_byName(product.getName()).getMainImageURL();
        ImageBean image = new ImageBean();
        String name_new=file.getSubmittedFileName();

            Path path = Paths.get(addressPathImage+file.getSubmittedFileName());
            if (Files.exists(path)) {
                 // file exist-> delete file
                image.deleteFile(path.toString());
                image.changeImageProduct(file, addressPathImage, name_product, name_new);
                messimgproduct = " Change Image successfully!";
             
            }else{
                image.changeImageProduct(file, addressPathImage, name_product, name_new);
                messimgproduct = " Change Image successfully!";
            }
        }
        
//    }
    
    
    //ajax
    //get all parent category ,using for create new product page
    public void getCategoryAjax(AjaxBehaviorEvent event) throws SQLException, ClassNotFoundException, InstantiationException, IllegalAccessException {
        CategoryBean ab = new CategoryBean();
        listcategory = ab.getCategoriesByParent(product.getId_parent());
    }
    
    //get image product by name, using for change image product page
    public void getImageAjax(AjaxBehaviorEvent event) throws SQLException, ClassNotFoundException, InstantiationException, IllegalAccessException {
        
        imageajax= getProduct_byName(product.getName()).getMainImageURL();
    }
    

// SET & GET///////////////////////////////////////////////////////////////////////////////////
    public Integer getId_product() {
        return product.getId_product();
    }

    public void setId_product(Integer id_product) {
        product.setId_product(id_product);
    }

    public String getName() {
        return product.getName();
    }

    public void setName(String name) {
        product.setName(name);
    }

    public String getIntroduce() {
        return product.getIntroduce();
    }

    public void setIntroduce(String introduce) {
        product.setIntroduce(introduce);
    }

    public Integer getCategory_id() {
        return product.getCategory_id();
    }

    public void setCategory_id(Integer category_id) {
        product.setCategory_id(category_id);
    }

    public Integer getDetail_DetailID() {
        return product.getDetail_DetailID();
    }

    public void setDetail_DetailID(Integer detail_DetailID) {
        product.setDetail_DetailID(detail_DetailID);
    }

    public Integer getQuantity() {
        return product.getQuantity();
    }

    public void setQuantity(Integer quantity) {
        product.setQuantity(quantity);
    }

    public Long getPrice() {
        return product.getPrice();
    }

    public void setPrice(Long price) {
        product.setPrice(price);
    }

    public Integer getPrice_promotion() {
        return product.getPrice_promotion();
    }

    public void setPrice_promotion(Integer price_promotion) {
        product.setPrice_promotion(price_promotion);
    }

    public String getStatus() {
        return product.getStatus();
    }

    public void setStatus(String status) {
        product.setStatus(status);
    }

    public Date getCreate_date() {
        return product.getCreate_date();
    }

    public void setCreate_date(Date create_date) {
        product.setCreate_date(create_date);
    }

    public Date getUpdate_date() {
        return product.getUpdate_date();
    }

    public void setUpdate_date(Date update_date) {
        product.setUpdate_date(update_date);
    }

    public Date getPublic_date() {
        return product.getPublic_date();
    }

    public void setPublic_date(Date public_date) {
        product.setPublic_date(public_date);
    }

    public String getManufacter() {
        return product.getManufacter();
    }

    public void setManufacter(String manufacter) {
        product.setManufacter(manufacter);
    }

    public String getModel() {
        return product.getModel();
    }

    public void setModel(String model) {
        product.setModel(model);
    }

    public Integer getSoldquantity() {
        return product.getSoldquantity();
    }

    public void setSoldquantity(Integer soldquantity) {
        product.setSoldquantity(soldquantity);
    }

    public String getMainImageURL() {
        return product.getMainImageURL();
    }

    public void setMainImageURL(String MainImageURL) {
        product.setMainImageURL(MainImageURL);
    }

    public Integer getViews() {
        return product.getViews();
    }

    public void setViews(Integer views) {
        product.setViews(views);
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public Integer getNum() {
        return num;
    }

    public void setNum(Integer num) {
        this.num = num;
    }

    public Integer getHide() {
        return product.getHide();
    }
    
    public void setHide(Integer hide) {
        product.setHide(hide);
    }

    public Integer getId_parent() {
        return product.getId_parent();
    }

    public void setId_parent(Integer id_parent) {
        product.setId_parent(id_parent);
    }

    
    public String getMes() {
        return mes;
    }

    public void setMes(String mes) {
        this.mes = mes;
    }

    public String getNamenew() {
        return namenew;
    }

    public void setNamenew(String namenew) {
        this.namenew = namenew;
    }

    public Integer getCate_idnew() {
        return cate_idnew;
    }

    public void setCate_idnew(Integer cate_idnew) {
        this.cate_idnew = cate_idnew;
    }

    public Long getPricenew() {
        return pricenew;
    }

    public void setPricenew(Long pricenew) {
        this.pricenew = pricenew;
    }

    public Integer getPromotionnew() {
        return promotionnew;
    }

    public void setPromotionnew(Integer promotionnew) {
        this.promotionnew = promotionnew;
    }

    public Integer getQuantitynew() {
        return quantitynew;
    }

    public void setQuantitynew(Integer quantitynew) {
        this.quantitynew = quantitynew;
    }

    public String getManufaternew() {
        return manufaternew;
    }

    public void setManufaternew(String manufaternew) {
        this.manufaternew = manufaternew;
    }

    public Integer getHidenew() {
        return hidenew;
    }

    public void setHidenew(Integer hidenew) {
        this.hidenew = hidenew;
    }

    public ArrayList<Category> getListcategory() {
        return listcategory;
    }

    public void setListcategory(ArrayList<Category> listcategory) {
        this.listcategory = listcategory;
    }

    public Integer getHidden1() {
        return product.getHidden1();
    }

    public void setHidden1(Integer hidden1) {
        product.setHidden1(hidden1);
    }

    public Integer getHidden2() {
        return product.getHidden2();
    }

    public void setHidden2(Integer hidden2) {
        product.setHidden2(hidden2);
    }

    public Integer getPd_ID() {
        return pd_ID;
    }

    public void setPd_ID(Integer pd_ID) {
        this.pd_ID = pd_ID;
    }

    public ArrayList<Category> getListcate() {
        return listcate;
    }

    public void setListcate(ArrayList<Category> listcate) {
        this.listcate = listcate;
    }

    public Integer getViewnum() {
        return viewnum;
    }

    public void setViewnum(Integer viewnum) {
        this.viewnum = viewnum;
    }

    public ArrayList<Product> getListproductbycate() {
        return listproductbycate;
    }

    public void setListproductbycate(ArrayList<Product> listproductbycate) {
        this.listproductbycate = listproductbycate;
    }

    public String getCate() {
        return cate;
    }

    public void setCate(String cate) {
        this.cate = cate;
    }

    public String getImageajax() {
        return imageajax;
    }

    public void setImageajax(String imageajax) {
        this.imageajax = imageajax;
    }

    public Part getFile() {
        return file;
    }

    public void setFile(Part file) {
        this.file = file;
    }

    public String getMessimgproduct() {
        return messimgproduct;
    }

    public void setMessimgproduct(String messimgproduct) {
        this.messimgproduct = messimgproduct;
    }

    
    
 
    //        Path path = Paths.get(addressPathImage+file.getSubmittedFileName());
//
//        if (Files.exists(path)) {
//            
//            image.deleteFile(addressPathImage+path.getFileName());
//          // file exist
//        }

//        String name_product=getProduct_byName(product.getName()).getName();
    
    //    public String addCart() throws SQLException, ClassNotFoundException, InstantiationException, IllegalAccessException {
//        Map<String, String> map = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
//        Integer productID = Integer.valueOf(map.get("id"));
//        Integer userID = Integer.valueOf(map.get("id_user"));
//        Integer quantity = Integer.valueOf(map.get("quantity"));
//        HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
////        session.setAttribute("productID"+productID,productID);
////        session.setAttribute("userID"+userID,userID);
////        session.setAttribute("quantity"+quantity,quantity);
//        session.setAttribute("pd",ProductBean.this.getProduct_byID(productID));
////        ProductDao dao=new ProductDao();
////        dao.insertCart(productID,userID ,quantity );
//        return "homepage_1?faces-redirect=true";
//    }
    
        //get views of product ajax ,using for detail product page
//    public void getViewsAjax(AjaxBehaviorEvent event) throws SQLException, ClassNotFoundException, InstantiationException, IllegalAccessException {
//        viewnum = getViews(product.getId_product())+1;
//    }
    //get views of product by id
//    public Integer getViews(Integer id) throws SQLException, ClassNotFoundException, InstantiationException, IllegalAccessException{
//        ProductDao dao=new ProductDao();
//        return dao.getProduct_by_id(id).getViews();
//    }
//    public String getCategory(String param) {
//        String cat = "";
//        switch (param) {
//            case "dienthoai":
//                cat = "Điện thoại";
//                break;
//            case "laptop":
//                cat = "Laptop";
//                break;
//            case "maytinhbang":
//                cat = "Máy tính bảng";
//                break;
//            case "phukien":
//                cat = "Phụ kiện";
//                break;
//            case "tintuc":
//                cat = "Tin Tức";
//                break;
//            default:
//                break;
//        }
//        return cat;
//    }
    
}
