/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mobizi.controller;

import com.mobizi.dao.UserDao;
import com.mobizi.entity.User;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

/**
 *
 * @author tran
 */
@ManagedBean
@SessionScoped
public class LoginBean {

    private User user = new User();
    private String error;

    public LoginBean() {

    }

    //check username
    public boolean checkLogin()
            throws SQLException, ClassNotFoundException, InstantiationException, IllegalAccessException {
        User item = getUser_by_username(user.getUsername());
        if (item.getUsername() != null && item.getPassword().equals(user.getPassword())) {
            return true;
        }
        return false;
    }

    //get user by username
    public User getUser_by_username(String username) throws SQLException, ClassNotFoundException, InstantiationException, IllegalAccessException {
        UserDao dao = new UserDao();
        return dao.getUser_by_username(username);
    }

    // login
    public String login() throws SQLException, ClassNotFoundException, InstantiationException, IllegalAccessException {

        if (checkLogin()) {
            boolean st=getUser_by_username(user.getUsername()).getStatus().equalsIgnoreCase("baned");
           if( getUser_by_username(user.getUsername()).getStatus().equalsIgnoreCase("baned")==false){
                HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
                User entity = getUser_by_username(user.getUsername());
                session.setAttribute("id_user",entity.getId_user());//kiem tra lai???
                session.setAttribute("username", entity.getUsername());
                session.setAttribute("role", entity.getRole());
                error="";
                user.setMail(entity.getMail());
                user.setAddress(entity.getAddress());
                user.setBirthday(entity.getBirthday());
                user.setCity(entity.getCity());
                user.setFull_name(entity.getFull_name());
                user.setGender(entity.getGender());
                user.setLast_access_date(entity.getLast_access_date());
                user.setPicture(entity.getPicture());
                user.setTel(entity.getTel());
                user.setRole(entity.getRole());
                user.setUsername(entity.getUsername());
                user.setStatus(entity.getStatus()); 
                if(checkRole()){
                    return "admin/admin_page?faces-redirect=true";
                }else{
                    return "member_page?faces-redirect=true";
                }
           }else{
               return "isbanned?faces-redirect=true";
           }
        } else {
            error = "Error: Username or password is wrong";
            return "login?faces-redirect=true";
        }
    }
    //log out
    public String logout() {
//        HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
//        session.removeAttribute("username");
//        session.removeAttribute("id_user");
//        session.removeAttribute("role");
//        user.setUsername(null);
//        UserBean use=new UserBean();
//        use.setMessagei("");
//        use.setMessageimage("");
//        use.setMessagep("");
        FacesContext.getCurrentInstance().getExternalContext().invalidateSession();
        return "homepage_1?faces-redirect=true";
    }
    
     // login
    public String login_Admin() throws SQLException, ClassNotFoundException, InstantiationException, IllegalAccessException {

        if (checkLogin()) {
            HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
            
            User entity = getUser_by_username(user.getUsername());
            session.setAttribute("id_user",entity.getId_user());//kiem tra lai???
            session.setAttribute("username", entity.getUsername());
            error="";
            session.setAttribute("role", entity.getRole());
            user.setMail(entity.getMail());
            user.setAddress(entity.getAddress());
            user.setBirthday(entity.getBirthday());
            user.setCity(entity.getCity());
            user.setFull_name(entity.getFull_name());
            user.setGender(entity.getGender());
            user.setLast_access_date(entity.getLast_access_date());
            user.setPicture(entity.getPicture());
            user.setTel(entity.getTel());
            user.setRole(entity.getRole());
            user.setUsername(entity.getUsername());
            user.setStatus(entity.getStatus()); 
            if(checkRole()){         
                    return "list_product?faces-redirect=true";
            }else {return null;}
        }else {
            error = "Error: Username or password is wrong";
            return "login?faces-redirect=true";
        }
        
    }
   
    public String logout_Admin() {
//        HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
//        session.removeAttribute("username");
//        session.removeAttribute("id_user");
//        session.removeAttribute("role");
//        
//        user.setUsername(null);
        FacesContext.getCurrentInstance().getExternalContext().invalidateSession();
        return "login?faces-redirect=true";
    }
   
    //check session - true:  logined
    public boolean checkSession() {
        HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
        if (session.getAttribute("username") == null) {
            return false;
        } else {
            return true;
        }
    }
    
    //check role
    public boolean checkRole(){
        HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
        if ((Integer) session.getAttribute("role") == 1) {
            return true;
        }else {
            return  false;
        }
        
    }
    
    //to display member or admin
    public String checkType(){
        HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
        if ((Integer) session.getAttribute("role") == 1) {
            return "Administrator";
        }else {
            return  "Member";
        }
    }
    //SET & GET

    public Integer getId_user() {
        return user.getId_user();
    }

    public void setId_user(Integer id_user) {
        user.setId_user(id_user);
    }

    public String getMail() {
        return user.getMail();
    }

    public void setMail(String mail) {
        user.setMail(mail);
    }

    public String getPassword() {
        return user.getPassword();
    }

    public void setPassword(String password) {
        user.setPassword(password);
    }

    public String getUsername() {
        return user.getUsername();
    }

    public void setUsername(String username) {
        user.setUsername(username);
    }

    public Date getBirthday() {
        return user.getBirthday();
    }

    public void setBirthday(Date birthday) {
        user.setBirthday(birthday);
    }

    public String getTel() {
        return user.getTel();
    }

    public void setTel(String tel) {
        user.setTel(tel);
    }

    public String getStatus() {
        return user.getStatus();
    }

    public void setStatus(String status) {
        user.setStatus(status);
    }

    public String getPicture() {
        return user.getPicture();
    }

    public void setPicture(String picture) {
        user.setPicture(picture);
    }

    public Date getLast_access_date() {
        return user.getLast_access_date();
    }

    public void setLast_access_date(Date last_access_date) {
        user.setLast_access_date(last_access_date);
    }

    

    public String getCity() {
        return user.getCity();
    }

    public void setCity(String city) {
        user.setCity(city);
    }

    public String getAddress() {
        return user.getAddress();
    }

    public void setAddress(String address) {
        user.setAddress(address);
    }

    public String getGender() {
        return user.getGender();
    }

    public void setGender(String gender) {
        user.setGender(gender);
    }

    

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getFull_name() {
        return user.getFull_name();
    }

    public void setFull_name(String full_name) {
        user.setFull_name(full_name);
    }

}
