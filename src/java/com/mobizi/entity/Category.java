/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mobizi.entity;

/**
 *
 * @author tran
 */
public class Category {
    private Integer id_category;
    private String name;
    private Integer id_parent;

 
    public Integer getId_category() {
        return id_category;
    }

    public void setId_category(Integer id_category) {
        this.id_category = id_category;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getId_parent() {
        return id_parent;
    }

    public void setId_parent(Integer id_parent) {
        this.id_parent = id_parent;
    }
    
}
