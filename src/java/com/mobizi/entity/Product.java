/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mobizi.entity;

import java.math.BigDecimal;
import java.util.Date;
import javax.annotation.Generated;

/**
 *
 * @author tran
 */
public class Product {
    private Integer id_product;
    private String name;
    private String introduce;
    private Integer category_id;
    private Integer detail_DetailID;
    private Integer id_parent;
    private Integer quantity;
    private Long price;
    private Integer price_promotion;
    private String status;
    private Date create_date;
    private Date update_date;
    private Date public_date;
    private String manufacter;
    private String model;
    private Integer soldquantity;
    private String mainImageURL;
    private Integer views;
    private Integer hide;
    private Integer hidden1;
    private Integer hidden2;
    private Long temp;
    private Long temp1;
    
    public Integer getId_product() {
        return id_product;
    }
    
    public void setId_product(Integer id_product) {
        this.id_product = id_product;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIntroduce() {
        return introduce;
    }

    public void setIntroduce(String introduce) {
        this.introduce = introduce;
    }

    public Integer getCategory_id() {
        return category_id;
    }

    public void setCategory_id(Integer category_id) {
        this.category_id = category_id;
    }

    public Integer getDetail_DetailID() {
        return detail_DetailID;
    }

    public void setDetail_DetailID(Integer detail_DetailID) {
        this.detail_DetailID = detail_DetailID;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public Long getPrice() {
        return price;
    }

    public void setPrice(Long price) {
        this.price = price;
    }

    public Integer getPrice_promotion() {
        return price_promotion;
    }

    public void setPrice_promotion(Integer price_promotion) {
        this.price_promotion = price_promotion;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getCreate_date() {
        return create_date;
    }

    public void setCreate_date(Date create_date) {
        this.create_date = create_date;
    }

    public Date getUpdate_date() {
        return update_date;
    }

    public void setUpdate_date(Date update_date) {
        this.update_date = update_date;
    }

    public Date getPublic_date() {
        return public_date;
    }

    public void setPublic_date(Date public_date) {
        this.public_date = public_date;
    }

    public String getManufacter() {
        return manufacter;
    }

    public void setManufacter(String manufacter) {
        this.manufacter = manufacter;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public Integer getSoldquantity() {
        return soldquantity;
    }

    public void setSoldquantity(Integer soldquantity) {
        this.soldquantity = soldquantity;
    }

    public String getMainImageURL() {
        return mainImageURL;
    }

    public void setMainImageURL(String mainImageURL) {
        this.mainImageURL = mainImageURL;
    }

    public Integer getViews() {
        return views;
    }

    public void setViews(Integer views) {
        this.views = views;
    }

    public Integer getHide() {
        return hide;
    }

    public void setHide(Integer hide) {
        this.hide = hide;
    }

    public Long getTemp() {
        return temp;
    }

    public void setTemp(Long temp) {
        this.temp = temp;
    }

    public Integer getId_parent() {
        return id_parent;
    }

    public void setId_parent(Integer id_parent) {
        this.id_parent = id_parent;
    }

    public Integer getHidden1() {
        return hidden1;
    }

    public void setHidden1(Integer hidden1) {
        this.hidden1 = hidden1;
    }

    public Integer getHidden2() {
        return hidden2;
    }

    public void setHidden2(Integer hidden2) {
        this.hidden2 = hidden2;
    }

    public Long getTemp1() {
        return temp1;
    }

    public void setTemp1(Long temp1) {
        this.temp1 = temp1;
    }
    
}
