/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mobizi.entity;

/**
 * Product toi gian
 *
 * @author tran
 */
public class SubProduct {

    private String productID;
    
    private String userID;

    public SubProduct(String productID,String userID)  {
        this.productID = productID;
        this.userID = userID;
    }

    public String getProductID() {
        return productID;
    }

    public void setProductID(String productID) {
        this.productID = productID;
    }

   

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

}
