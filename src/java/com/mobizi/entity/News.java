/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mobizi.entity;

import java.util.Date;

/**
 *
 * @author tran
 */
public class News {
    private Integer id_new;
    private String title;
    private String content;
    private Date public_date;
    private Integer hide;
    private Integer views;

    
    
    //SET GET
    public Integer getId_new() {
        return id_new;
    }

    public void setId_new(Integer id_new) {
        this.id_new = id_new;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Date getPublic_date() {
        return public_date;
    }

    public void setPublic_date(Date public_date) {
        this.public_date = public_date;
    }

    public Integer getHide() {
        return hide;
    }

    public void setHide(Integer hide) {
        this.hide = hide;
    }

    public Integer getViews() {
        return views;
    }

    public void setViews(Integer views) {
        this.views = views;
    }
    
    
    
}
