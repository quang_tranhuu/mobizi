/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mobizi.entity;

import java.util.Date;

/**
 *
 * @author tran
 */
public class Ordertable {
    
    private Integer id_order;
    private Integer user_id;
    private Date order_date;
    private Long order_total;
    private Date delivery_date;
    private String stat;
    private String receiver;
    private String address_order;
    private Long total_price;
    private String description;
    private String tel_receiver;

    public Ordertable(){
    }
    
    public Ordertable(Integer id_order, Integer user_id, Date order_date, Long order_total, Date delivery_date, String stat, String receiver, String address_order, Long total_price, String description,String tel_receiver) {
        this.id_order = id_order;
        this.user_id = user_id;
        this.order_date = order_date;
        this.order_total = order_total;
        this.delivery_date = delivery_date;
        this.stat = stat;
        this.receiver = receiver;
        this.address_order = address_order;
        this.total_price = total_price;
        this.description = description;
        this.tel_receiver=tel_receiver;
    }

    public Integer getId_order() {
        return id_order;
    }

    public void setId_order(Integer id_order) {
        this.id_order = id_order;
    }

    public Integer getUser_id() {
        return user_id;
    }

    public void setUser_id(Integer user_id) {
        this.user_id = user_id;
    }

    public Date getOrder_date() {
        return order_date;
    }

    public void setOrder_date(Date order_date) {
        this.order_date = order_date;
    }

    public Long getOrder_total() {
        return order_total;
    }

    public void setOrder_total(Long order_total) {
        this.order_total = order_total;
    }

    public Date getDelivery_date() {
        return delivery_date;
    }

    public void setDelivery_date(Date delivery_date) {
        this.delivery_date = delivery_date;
    }

    public String getStat() {
        return stat;
    }

    public void setStat(String stat) {
        this.stat = stat;
    }

    public String getReceiver() {
        return receiver;
    }

    public void setReceiver(String receiver) {
        this.receiver = receiver;
    }

    public String getAddress_order() {
        return address_order;
    }

    public void setAddress_order(String address_order) {
        this.address_order = address_order;
    }

    public Long getTotal_price() {
        return total_price;
    }

    public void setTotal_price(Long total_price) {
        this.total_price = total_price;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTel_receiver() {
        return tel_receiver;
    }

    public void setTel_receiver(String tel_receiver) {
        this.tel_receiver = tel_receiver;
    }

    
    
}
