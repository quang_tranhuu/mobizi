/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mobizi.dao;

import com.mobizi.connect.ConnectionFactory;
import com.mobizi.entity.Category;
import com.mobizi.entity.News;
import com.mobizi.entity.Product;
import com.mobizi.entity.User;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author tran
 */
public class ProductDao {

    public ProductDao() {
    }

    //list all product
    public ArrayList<Product> getAllProduct() throws SQLException, ClassNotFoundException, InstantiationException, IllegalAccessException {
        String sql = "SELECT * FROM product";
        return fetchproduct(sql);

    }

    //get product by id
    public Product getProduct_by_id(Integer id) throws SQLException, ClassNotFoundException, InstantiationException, IllegalAccessException {
        try (Connection conn = ConnectionFactory.getConnection()) {
            String sql = "SELECT * FROM product WHERE id_product=" + id;
            try (PreparedStatement ps = conn.prepareStatement(sql)) {
                Product entity = new Product();
                ResultSet rs = ps.executeQuery(sql);
                while (rs.next()) {
                    entity.setId_product(rs.getInt("id_product"));
                    entity.setName(rs.getString("name"));
                    entity.setIntroduce(rs.getString("introduce"));
                    entity.setCategory_id(rs.getInt("category_id"));
                    entity.setDetail_DetailID(rs.getInt("detail_DetailID"));
                    entity.setPrice(rs.getLong("price"));
                    entity.setQuantity(rs.getInt("quantity"));
                    entity.setPrice_promotion(rs.getInt("price_promotion"));
                    entity.setStatus(rs.getString("status"));
                    entity.setCreate_date(rs.getTimestamp("create_date"));
                    entity.setUpdate_date(rs.getTimestamp("update_date"));
                    entity.setPublic_date(rs.getTimestamp("public_date"));
                    entity.setManufacter(rs.getString("manufacter"));
                    entity.setModel(rs.getString("model"));
                    entity.setSoldquantity(rs.getInt("soldquantity"));
                    entity.setViews(rs.getInt("views"));
                    entity.setMainImageURL(rs.getString("mainImageURL"));
                    entity.setHide(rs.getInt("hide"));
                    entity.setTemp(rs.getLong("temp"));
                }
                return entity;
            }
        }
    }

    
    //get product by name
    public Product getProduct_by_Name(String name) throws SQLException, ClassNotFoundException, InstantiationException, IllegalAccessException {
        try (Connection conn = ConnectionFactory.getConnection()) {
            String sql = "SELECT * FROM product WHERE name='" + name+"'";
            try (PreparedStatement ps = conn.prepareStatement(sql)) {
                Product entity = new Product();
                ResultSet rs = ps.executeQuery(sql);
                while (rs.next()) {
                    entity.setId_product(rs.getInt("id_product"));
                    entity.setName(rs.getString("name"));
                    entity.setIntroduce(rs.getString("introduce"));
                    entity.setCategory_id(rs.getInt("category_id"));
                    entity.setDetail_DetailID(rs.getInt("detail_DetailID"));
                    entity.setPrice(rs.getLong("price"));
                    entity.setQuantity(rs.getInt("quantity"));
                    entity.setPrice_promotion(rs.getInt("price_promotion"));
                    entity.setStatus(rs.getString("status"));
                    entity.setCreate_date(rs.getTimestamp("create_date"));
                    entity.setUpdate_date(rs.getTimestamp("update_date"));
                    entity.setPublic_date(rs.getTimestamp("public_date"));
                    entity.setManufacter(rs.getString("manufacter"));
                    entity.setModel(rs.getString("model"));
                    entity.setSoldquantity(rs.getInt("soldquantity"));
                    entity.setViews(rs.getInt("views"));
                    entity.setMainImageURL(rs.getString("mainImageURL"));
                    entity.setHide(rs.getInt("hide"));
                    entity.setTemp(rs.getLong("temp"));
                }
                return entity;
            }
        }
    }
    
    //list product
    public ArrayList<Product> fetchproduct(String sql) throws SQLException, ClassNotFoundException, InstantiationException, IllegalAccessException {
        try (Connection conn = ConnectionFactory.getConnection()) {

            try (PreparedStatement ps = conn.prepareStatement(sql)) {
                ArrayList<Product> list = new ArrayList<>();
                ResultSet rs = ps.executeQuery(sql);
                while (rs.next()) {
                    Product entity = new Product();
                    entity.setId_product(rs.getInt("id_product"));
                    entity.setName(rs.getString("name"));
                    entity.setIntroduce(rs.getString("introduce"));
                    entity.setCategory_id(rs.getInt("category_id"));
                    entity.setDetail_DetailID(rs.getInt("detail_DetailID"));
                    entity.setPrice(rs.getLong("price"));
                    entity.setQuantity(rs.getInt("quantity"));
                    entity.setPrice_promotion(rs.getInt("price_promotion"));
                    entity.setStatus(rs.getString("status"));
                    entity.setCreate_date(rs.getTimestamp("create_date"));
                    entity.setUpdate_date(rs.getTimestamp("update_date"));
                    entity.setPublic_date(rs.getTimestamp("public_date"));
                    entity.setManufacter(rs.getString("manufacter"));
                    entity.setModel(rs.getString("model"));
                    entity.setSoldquantity(rs.getInt("soldquantity"));
                    entity.setViews(rs.getInt("views"));
                    entity.setMainImageURL(rs.getString("MainImageURL"));
                    entity.setHide(rs.getInt("hide"));
                    entity.setTemp(rs.getLong("temp"));
                    list.add(entity);

                }
                return list;
            }
        }
    }



    //get hot product 
    public ArrayList<Product> getHot(Integer num) throws SQLException, ClassNotFoundException, InstantiationException, IllegalAccessException {
        String sql = "SELECT * FROM product ORDER by views DESC LIMIT " + num;

        return fetchproduct(sql);

    }

    //get hot product by id_parent
    public ArrayList getHot_by_Category(Integer num, Integer id_parent) throws SQLException, ClassNotFoundException, InstantiationException, IllegalAccessException {
        if (id_parent >= 0 && id_parent <= 3) {
            String sql = "SELECT p.*  FROM product as p LEFT JOIN category as c ON c.id_category=p.category_id WHERE id_parent=" + id_parent + " ORDER by views DESC LIMIT " + num;
            System.out.print("xxxxxxxxx:" + sql);
            return fetchproduct(sql);
        }
        return null;
    }

    //insert new product
    public void insertNew_Product(String name,Integer cate_id,Long price,Integer promotion,String manufacter,Integer quantity,Integer hide,String mainImageURL) throws SQLException, ClassNotFoundException, InstantiationException, IllegalAccessException{
        try (Connection conn = ConnectionFactory.getConnection()) {
            String sql = "INSERT INTO product (name,category_id,price,price_promotion,manufacter,quantity,hide,mainImageURL)"
                    + " VALUES ('"+name+"',"+cate_id+","+price+","+promotion+",'"+manufacter+"',"+quantity+","+hide+",'"+mainImageURL+"')";
           System.out.print("xxxxX:" +sql);
            try (PreparedStatement ps = conn.prepareStatement(sql)) {

                ps.executeUpdate(sql);
            }
        }
    }
    //get all product by category
    public ArrayList getAllProductByCategory(String category_parent) throws SQLException, ClassNotFoundException, InstantiationException, IllegalAccessException {
        Integer id = 0;
        switch (category_parent) {
            case "điện thoại":
                id = 0;
                break;
            case "laptop":
                id = 1;
                break;
            case "máy tính bảng":
                id = 2;
                break;
            case "phụ kiện":
                id = 3;
                break;
            default:
                break;
        }
            String sql = "SELECT p.*  FROM product as p LEFT JOIN category as c ON c.id_category=p.category_id WHERE id_parent=" + id;
            return fetchproduct(sql);
    }

    
    //get all product by category_id
    public ArrayList getAllProduct_By_SubCategory(Integer category) throws SQLException, ClassNotFoundException, InstantiationException, IllegalAccessException {

        String sql = "SELECT * FROM product WHERE category_id=" +category;
//        String sql = "SELECT p.*  FROM product as p LEFT JOIN category as c ON c.id_category=p.category_id WHERE c.id_category=" + category;
            return fetchproduct(sql);

    }
// Insert product to cart

    public void insertCart(Integer id, Integer id_user, Integer quantity) throws SQLException, ClassNotFoundException, InstantiationException, IllegalAccessException {
        Product item = getProduct_by_id(id);
        try (Connection conn = ConnectionFactory.getConnection()) {
            String sql = "INSERT INTO orderline (product_id,quantity,user_id) VALUES (" + item.getId_product() + "," + quantity + "," + id_user + ")";

            try (PreparedStatement ps = conn.prepareStatement(sql)) {

                ps.executeUpdate(sql);
            }
        }
    }

    //update product by id
    public void update_Product(Integer id,String name,Integer cate_id,Long price,Integer promotion,String manufacter,Integer quantity,Integer hide) throws SQLException, ClassNotFoundException, InstantiationException, IllegalAccessException {
//        Product pd = getProduct_by_id(id);
        try (Connection conn = ConnectionFactory.getConnection()) {
            String sql = "UPDATE product SET name='" + name + "',category_id=" + cate_id
                    + ",price=" + price + ",price_promotion=" + promotion
                    + ",manufacter='" + manufacter + "',quantity=" + quantity + ",hide=" + hide +" WHERE id_product="+id;
           System.out.print("xxxxX:" +sql);
            try (PreparedStatement ps = conn.prepareStatement(sql)) {

                ps.executeUpdate(sql);
            }
        }
    }

    //delete product by id
    public void delete_Product_by_ID(Integer id) throws SQLException, ClassNotFoundException, InstantiationException, IllegalAccessException {
        try (Connection conn = ConnectionFactory.getConnection()) {
            String sql = "DELETE FROM product WHERE id_product=" + id;
            try (PreparedStatement ps = conn.prepareStatement(sql)) {
                ps.executeUpdate();
            }
        }
    }
    //increase views
    public void update_Views(Integer id_product,Integer views) throws SQLException, ClassNotFoundException, InstantiationException, IllegalAccessException{
        try (Connection conn = ConnectionFactory.getConnection()) {
            views+=1;
            String sql = "UPDATE product SET views=" + views +" WHERE id_product="+id_product;
           System.out.print("xxxxX:" +sql);
            try (PreparedStatement ps = conn.prepareStatement(sql)) {

                ps.executeUpdate(sql);
            }
        }
    }
    //update image product
    public void changePicture(String  name_product,String image) throws SQLException, ClassNotFoundException, InstantiationException, IllegalAccessException{
        try (Connection conn = ConnectionFactory.getConnection()) {
            String sql = "UPDATE product SET mainImageURL='" + image +"' WHERE name='"+name_product+"'";
           System.out.print("xxxxX:" +sql);
            try (PreparedStatement ps = conn.prepareStatement(sql)) {
                ps.executeUpdate(sql);
            }
        }
    }
    
    
    
    
    //    public ArrayList<News> fetchpnews(String sql) throws SQLException, ClassNotFoundException, InstantiationException, IllegalAccessException {
//        try (Connection conn = ConnectionFactory.getConnection()) {
//
//            try (PreparedStatement ps = conn.prepareStatement(sql)) {
//                ArrayList<News> list = new ArrayList<>();
//                ResultSet rs = ps.executeQuery(sql);
//                while (rs.next()) {
//                    News entity = new News();
//                    entity.setId_new(rs.getInt("id_new"));
//                    entity.setTitle(rs.getString("title"));
//                    entity.setHide(rs.getInt("hide"));
//                    entity.setContent(rs.getString("content"));
//                    entity.setPublic_date(rs.getDate("public_date"));
//                    entity.setViews(rs.getInt("views"));
//                    list.add(entity);
//
//                }
//                return list;
//            }
//        }
//    }
}
