/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mobizi.dao;

import com.mobizi.connect.ConnectionFactory;
import com.mobizi.entity.User;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author tran
 */
public class UserDao {

    public UserDao() {
    }

    //get list user
    public ArrayList<User> fetch_arraylist(String sql) throws SQLException, ClassNotFoundException, InstantiationException, IllegalAccessException {
        try (Connection conn = ConnectionFactory.getConnection()) {

            try (PreparedStatement ps = conn.prepareStatement(sql)) {
                ArrayList<User> list = new ArrayList<>();
                ResultSet rs = ps.executeQuery(sql);
                while (rs.next()) {
                    User entity = new User();
                    entity.setId_user(rs.getInt("id_user"));
                    entity.setUsername(rs.getString("username"));
                    entity.setMail(rs.getString("mail"));
                    entity.setFull_name(rs.getString("full_name"));
                    entity.setPassword(rs.getString("password"));
                    entity.setBirthday(rs.getDate("birthday"));
                    entity.setGender(rs.getString("gender"));
                    entity.setCity(rs.getString("city"));
                    entity.setAddress(rs.getString("address"));
                    entity.setLast_access_date(rs.getDate("last_access_date"));
                    entity.setPicture(rs.getString("picture"));
                    entity.setStatus(rs.getString("status"));
                    entity.setRole(rs.getInt("role"));
                    list.add(entity);

                }
                return list;
            }
        }
    }
    //get one user
    public User fetch_user(String sql) throws SQLException, ClassNotFoundException, InstantiationException, IllegalAccessException {
        try (Connection conn = ConnectionFactory.getConnection()) {

            try (PreparedStatement ps = conn.prepareStatement(sql)) {
               User entity = new User();
                ResultSet rs = ps.executeQuery(sql);
                while (rs.next()) {
                    
                    entity.setId_user(rs.getInt("id_user"));
                    entity.setUsername(rs.getString("username"));
                    entity.setMail(rs.getString("mail"));
                    entity.setPassword(rs.getString("password"));
                    entity.setBirthday(rs.getDate("birthday"));
                    entity.setGender(rs.getString("gender"));
                    entity.setCity(rs.getString("city"));
                    entity.setAddress(rs.getString("address"));
                    entity.setLast_access_date(rs.getDate("last_access_date"));
                    entity.setPicture(rs.getString("picture"));
                    entity.setStatus(rs.getString("status"));
                    entity.setRole(rs.getInt("role"));
                   

                }
                return entity;
            }
        }
    }

    //get user by username
    public User getUser_by_username(String username) throws SQLException, ClassNotFoundException, InstantiationException, IllegalAccessException {
        try (Connection conn = ConnectionFactory.getConnection()) {
            String sql = "SELECT * FROM user WHERE username='" + username + "'";
            try (PreparedStatement ps = conn.prepareStatement(sql)) {
                User entity = new User();
                ResultSet rs = ps.executeQuery(sql);
                while (rs.next()) {

                    entity.setId_user(rs.getInt("id_user"));
                    entity.setUsername(rs.getString("username"));
                    entity.setFull_name(rs.getString("full_name"));
                    entity.setMail(rs.getString("mail"));
                    entity.setTel(rs.getString("tel"));
                    entity.setPassword(rs.getString("password"));
                    entity.setBirthday(rs.getDate("birthday"));
                    entity.setGender(rs.getString("gender"));
                    entity.setCity(rs.getString("city"));
                    entity.setAddress(rs.getString("address"));
                    entity.setLast_access_date(rs.getDate("last_access_date"));
                    entity.setPicture(rs.getString("picture"));
                    entity.setStatus(rs.getString("status"));
                    entity.setRole(rs.getInt("role"));
                }
                return entity;
            }
        }
    }
    //get user by email
    public User getUser_by_Mail(String mail) throws SQLException, ClassNotFoundException, InstantiationException, IllegalAccessException {
        
            String sql = "SELECT * FROM user WHERE mail='" + mail + "'";
            return fetch_user(sql);
                
            
    }

    //get all user
    public ArrayList<User> getAll_user() throws SQLException, ClassNotFoundException, InstantiationException, IllegalAccessException {
        String sql = "SELECT * FROM user";
        return fetch_arraylist(sql);
    }

    //add new user
    public void addUser(String username, String password, String mail) throws SQLException, ClassNotFoundException, InstantiationException, IllegalAccessException {
        try (Connection conn = ConnectionFactory.getConnection()) {
            String sql = "INSERT INTO user (username,password,mail,status) VALUES ('" + username + "','" + password + "','" + mail + "','active')";
            try (PreparedStatement ps = conn.prepareStatement(sql)) {
                ps.executeUpdate(sql);
            }
        }

    }

    //update information of user
    public void update_Info(Integer id_user,String full_name,String email,String tel,String city,String address,String gender) throws SQLException, ClassNotFoundException, InstantiationException, IllegalAccessException{
        try (Connection conn = ConnectionFactory.getConnection()) {
            String sql = "UPDATE user SET full_name='" + full_name + "',mail='" + email
                    + "',tel='" + tel + "',city='" + city
                    + "',address='" + address + "',gender='" + gender + "' WHERE id_user="+id_user;
            try (PreparedStatement ps = conn.prepareStatement(sql)) {

                ps.executeUpdate(sql);
            }
        }
    }
    
    //change password
    public void changePassword(String pas,Integer id_user) throws SQLException, ClassNotFoundException, InstantiationException, IllegalAccessException{
        try (Connection conn = ConnectionFactory.getConnection()) {
            String sql = "UPDATE user SET password='" + pas +"' WHERE id_user="+id_user;
           System.out.print("xxxxX:" +sql);
            try (PreparedStatement ps = conn.prepareStatement(sql)) {

                ps.executeUpdate(sql);
            }
        }
    }
    
    //change avatar
    public void changePicture(String username,String image) throws SQLException, ClassNotFoundException, InstantiationException, IllegalAccessException{
        try (Connection conn = ConnectionFactory.getConnection()) {
            String sql = "UPDATE user SET picture='" + image +"' WHERE username='"+username+"'";
           System.out.print("xxxxX:" +sql);
            try (PreparedStatement ps = conn.prepareStatement(sql)) {
                ps.executeUpdate(sql);
            }
        }
    }
    
    //delete user by id
    public void delete_User_By_ID(Integer id_user) throws SQLException, ClassNotFoundException, InstantiationException, IllegalAccessException{
        try (Connection conn = ConnectionFactory.getConnection()) {
            String sql = "DELETE FROM user WHERE id_user=" + id_user;
            try (PreparedStatement ps = conn.prepareStatement(sql)) {
                ps.executeUpdate();
            }
        }
    }
    //banned user by id
    public void banned_User_By_ID(Integer id_user) throws SQLException, ClassNotFoundException, InstantiationException, IllegalAccessException{
        try (Connection conn = ConnectionFactory.getConnection()) {
            String sql = "UPDATE user SET status='baned' WHERE id_user="+id_user;
            try (PreparedStatement ps = conn.prepareStatement(sql)) {
                ps.executeUpdate(sql);
            }
        }
    }
}
