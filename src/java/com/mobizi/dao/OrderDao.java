/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mobizi.dao;

import com.mobizi.connect.ConnectionFactory;
import com.mobizi.entity.Ordertable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author tran
 */
public class OrderDao {

    public OrderDao() {
    }
    
    //add order
    public void  addOrder(Integer user_id, Date order_date, Long order_total, Date delivery_date, String stat, String receiver, String address_order, Long total_price, String description,String tel) throws SQLException, ClassNotFoundException, InstantiationException, IllegalAccessException{
        String orderDate = "'" + new SimpleDateFormat("yyyy-MM-dd").format(order_date) + "'";
        String deliveryDate = "'" + new SimpleDateFormat("yyyy-MM-dd").format(delivery_date) + "'";
        try(Connection conn=ConnectionFactory.getConnection()){
    String sql="INSERT INTO ordertable "
            + "(user_id,order_date,order_total,delivery_date,stat,receiver,address_order,total_price,description,tel_receiver)"
            + " VALUES ("+user_id+","+orderDate+","+order_total+","+deliveryDate+",'"+stat+"','"+receiver+"','"+address_order+"',"+total_price+",'"+description+"','"+tel+"')";            
//    System.out.print("xxxxxxxxxxxxxxxxx: " + sql);
    try(PreparedStatement ps=conn.prepareStatement(sql)){
        ps.executeUpdate();
        
    }
        }
    }
    
    // list all order
    public ArrayList<Ordertable> list_All_Order() throws SQLException, ClassNotFoundException, InstantiationException, IllegalAccessException{
        try(Connection conn =ConnectionFactory.getConnection()){
            String sql="SELECT id_order,receiver,address_order,description,tel_receiver,order_date,delivery_date,stat FROM ordertable WHERE stat='process'";
            try(PreparedStatement ps=conn.prepareStatement(sql)){
               ResultSet rs =ps.executeQuery();
               ArrayList<Ordertable> list = new ArrayList<>();
               while (rs.next()) {          
                   Ordertable entity = new Ordertable();
                    entity.setId_order(rs.getInt("id_order"));
                    entity.setReceiver(rs.getString("receiver"));
                    entity.setAddress_order(rs.getString("address_order"));
//                    entity.setUser_id(rs.getInt("user_id"));
                    entity.setDelivery_date(rs.getDate("delivery_date"));
//                    entity.setOrder_total(rs.getLong("order_total"));
                    entity.setDescription(rs.getString("description"));
                    entity.setOrder_date(rs.getDate("order_date"));
                    entity.setStat(rs.getString("stat"));
                    entity.setTel_receiver(rs.getString("tel_receiver"));
//                    entity.setTotal_price(rs.getLong("total_price"));
                    list.add(entity);
                }
               return list;
            }
        }
    }
    
    //get order by id_user
    public ArrayList<Ordertable> getOrder_By_IDuser(Integer id_user) throws SQLException, ClassNotFoundException, InstantiationException, IllegalAccessException{
        try(Connection conn =ConnectionFactory.getConnection()){
            String sql="SELECT receiver,address_order,description,order_date,delivery_date,stat FROM ordertable WHERE user_id="+id_user;
            try(PreparedStatement ps=conn.prepareStatement(sql)){
               ResultSet rs =ps.executeQuery();
               ArrayList<Ordertable> list = new ArrayList<>();
               while (rs.next()) {          
                   Ordertable entity = new Ordertable();
                    entity.setReceiver(rs.getString("receiver"));
                    entity.setAddress_order(rs.getString("address_order"));
                    entity.setDelivery_date(rs.getDate("delivery_date"));
                    entity.setDescription(rs.getString("description"));
                    entity.setOrder_date(rs.getDate("order_date"));
                    entity.setStat(rs.getString("stat"));
                    list.add(entity);
                }
               return list;
            }
        }
    }
    //delete order by id
    public void delete_Order_by_id(Integer id) throws SQLException, ClassNotFoundException, InstantiationException, IllegalAccessException{
        try(Connection conn=ConnectionFactory.getConnection()){
        String sql="DELETE FROM ordertable WHERE id_order="+id;
            try(PreparedStatement ps=conn.prepareStatement(sql)){
                ps.executeUpdate();
            }
        }
    }
    
    //duyet order
    public void approval_Order(Integer id) throws SQLException, ClassNotFoundException, InstantiationException, IllegalAccessException{
        try(Connection conn=ConnectionFactory.getConnection()){
        String sql="UPDATE ordertable SET stat='checked' WHERE id_order="+id;
        System.out.print("xxxxxxxxxxxxxxxxx: " + sql);
            try(PreparedStatement ps=conn.prepareStatement(sql)){
                ps.executeUpdate();
            }
        }
    }
}
