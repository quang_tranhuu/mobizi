/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mobizi.dao;

import com.mobizi.connect.ConnectionFactory;
import com.mobizi.entity.Category;
import com.mobizi.entity.CategoryParent;
import java.sql.Array;
import java.util.ArrayList;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author tran
 */
public class CategoryDao {

    public CategoryDao() {
    }

    public ArrayList<Category> getAllCategory() throws SQLException, ClassNotFoundException, InstantiationException, IllegalAccessException {
        try (Connection conn = ConnectionFactory.getConnection()) {
            String sql = "SELECT * FROM category ";
            try (PreparedStatement ps = conn.prepareStatement(sql)) {
                ArrayList<Category> list = new ArrayList<>();
                ResultSet rs = ps.executeQuery(sql);
                while (rs.next()) {
                    Category entity = new Category();
                    entity.setId_category(rs.getInt("id_category"));
                    entity.setName(rs.getString("name"));
                    entity.setId_parent(rs.getInt("id_parent"));
                    list.add(entity);
                }
                return list;

            }
        }
    }
    public ArrayList<Category> getAll_SubCategory(Integer id_parent) throws SQLException, ClassNotFoundException, InstantiationException, IllegalAccessException {
        try (Connection conn = ConnectionFactory.getConnection()) {
            String sql = "SELECT * FROM category WHERE id_parent=" + id_parent;
            try (PreparedStatement ps = conn.prepareStatement(sql)) {
                ArrayList<Category> list = new ArrayList<>();
                ResultSet rs = ps.executeQuery(sql);
                while (rs.next()) {
                    Category entity = new Category();
                    entity.setId_category(rs.getInt("id_category"));
                    entity.setName(rs.getString("name"));
                    entity.setId_parent(rs.getInt("id_parent"));
                    list.add(entity);
                }
                return list;

            }
        }
    }
    public Category getCategory_by_IdCategory(Integer id_category) throws SQLException, ClassNotFoundException, InstantiationException, IllegalAccessException{
        try (Connection conn = ConnectionFactory.getConnection()) {
            String sql = "SELECT * FROM category WHERE id_category=" + id_category;
            try (PreparedStatement ps = conn.prepareStatement(sql)) {
                Category entity = new Category();
                ResultSet rs = ps.executeQuery(sql);
                while (rs.next()) {
                    entity.setId_category(rs.getInt("id_category"));
                    entity.setName(rs.getString("name"));
                    entity.setId_parent(rs.getInt("id_parent"));
                }
                return entity;
            }
        }
    }
    public ArrayList<Category> getCategory_By_Idparent(Integer id_parent) throws SQLException, ClassNotFoundException, InstantiationException, IllegalAccessException {
        try (Connection conn = ConnectionFactory.getConnection()) {
            String sql = "SELECT * FROM category WHERE id_parent=" + id_parent;
            try (PreparedStatement ps = conn.prepareStatement(sql)) {
                ArrayList<Category> list = new ArrayList<>();
                ResultSet rs = ps.executeQuery(sql);
                while (rs.next()) {
                    Category entity = new Category();
                    entity.setId_category(rs.getInt("id_category"));
                    entity.setName(rs.getString("name"));
                    entity.setId_parent(rs.getInt("id_parent"));
                    list.add(entity);
                }
                return list;

            }
        }
    }
    
    public ArrayList<CategoryParent> getAll_ParentCategory() throws SQLException, ClassNotFoundException, InstantiationException, IllegalAccessException{
        try (Connection conn = ConnectionFactory.getConnection()) {
            String sql = "SELECT * FROM categoryparent" ;
            try (PreparedStatement ps = conn.prepareStatement(sql)) {
                ArrayList list = new ArrayList<>();
                ResultSet rs = ps.executeQuery(sql);
                while (rs.next()) {
                    CategoryParent entity = new CategoryParent();
                    entity.setName(rs.getString("name"));
                    entity.setId(rs.getInt("id"));
                    list.add(entity);
                }
                return list;

            }
        }
        }
}
